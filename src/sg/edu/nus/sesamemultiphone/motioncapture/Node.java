package sg.edu.nus.sesamemultiphone.motioncapture;

public class Node {
	NodeType nodeType;
	double x, y;
	
	public Node(NodeType nodeType, double x, double y) {
		this.nodeType = nodeType;
		this.x = x;
		this.y = y;
	}
	
	public NodeType getNodeType() {
		return nodeType;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

	public Node reverse() {
		NodeType reversedNodeType = this.nodeType.reverse();
		return new Node(reversedNodeType, -getX(), getY());
	}
}
