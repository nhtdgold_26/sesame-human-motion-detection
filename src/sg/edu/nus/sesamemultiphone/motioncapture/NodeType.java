package sg.edu.nus.sesamemultiphone.motioncapture;

public enum NodeType {
	HEAD,
	HIP,
	LEFT_ELBOW,
	LEFT_HAND,
	RIGHT_ELBOW,
	RIGHT_HAND,
	LEFT_KNEE,
	LEFT_FOOT,
	RIGHT_KNEE,
	RIGHT_FOOT;
	
	public static int getTotalTypes() {
		return NodeType.values().length;
	}
	
	public NodeType reverse() {
		switch(this) {
		case LEFT_ELBOW: return NodeType.RIGHT_ELBOW;
		case LEFT_HAND: return NodeType.RIGHT_HAND;
		case RIGHT_ELBOW: return NodeType.LEFT_ELBOW;
		case RIGHT_HAND: return NodeType.LEFT_HAND;
		case LEFT_KNEE: return NodeType.RIGHT_KNEE;
		case LEFT_FOOT: return NodeType.RIGHT_FOOT;
		case RIGHT_KNEE: return NodeType.LEFT_KNEE;
		case RIGHT_FOOT: return NodeType.LEFT_FOOT;
		default: return this;
		}
	}
}