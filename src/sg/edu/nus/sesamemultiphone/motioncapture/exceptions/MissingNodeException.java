package sg.edu.nus.sesamemultiphone.motioncapture.exceptions;

@SuppressWarnings("serial")
public class MissingNodeException extends Exception{

	private final static String errorCode="MissingNode";

    public MissingNodeException(String message){
        super(errorCode + ": " + message);
    }

    public String getErrorCode(){
        return errorCode;
    }
}
