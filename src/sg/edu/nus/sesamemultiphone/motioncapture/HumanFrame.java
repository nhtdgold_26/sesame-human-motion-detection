package sg.edu.nus.sesamemultiphone.motioncapture;
//test
import java.util.HashMap;
import java.util.Iterator;

import sg.edu.nus.sesamemultiphone.motioncapture.exceptions.MissingNodeException;

public class HumanFrame {
	HashMap<NodeType, Node> _nodes = new HashMap<NodeType, Node>();

	public HumanFrame(Node[] nodes) throws MissingNodeException {
		checkSizeOf(nodes);
		_nodes = new HashMap<NodeType, Node>();
		for (Node node : nodes) {
			checkRepeating(node);
			_nodes.put(node.getNodeType(), node);
		}
	}

	public Node getNode(NodeType nodeType) {
		return _nodes.get(nodeType);
	}

	private void checkSizeOf(Node[] nodes) throws MissingNodeException {
		if (nodes.length != NodeType.getTotalTypes())
			throw new MissingNodeException(
					"The total number of nodes should be "
							+ NodeType.getTotalTypes() + " instead of "
							+ nodes.length);
	}

	private void checkRepeating(Node node) throws MissingNodeException {
		if (_nodes.containsKey(node.nodeType))
			throw new MissingNodeException("Repeated node type: "
					+ node.nodeType);
	}

	public HumanFrame reverseX() {
		Node[] nodes = new Node[NodeType.getTotalTypes()];
		Iterator<Node> nodeIterator = _nodes.values().iterator();
		for(int i=0; i<NodeType.getTotalTypes(); i++) {
			nodes[i] = nodeIterator.next().reverse();
		}
		try {
			return new HumanFrame(nodes);
		} catch (MissingNodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}