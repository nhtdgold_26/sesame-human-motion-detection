package sg.edu.nus.sesamemultiphone.motioncapture;

import java.util.ArrayList;

public class HumanTrajectory {
	private ArrayList<HumanFrame> humanFrames = new ArrayList<HumanFrame>();
	
	public void removeFirst() {
		humanFrames.remove(0);
	}
	
	public void addLast(HumanFrame humanFrame) {
		humanFrames.add(humanFrame);
	}
	
	public int getTotalFrames() {
		return humanFrames.size();
	}
	
	public HumanFrame getHumanFrame(int index) {
		return humanFrames.get(index);
	}
}