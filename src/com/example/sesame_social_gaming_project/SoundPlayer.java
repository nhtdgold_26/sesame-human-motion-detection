package com.example.sesame_social_gaming_project;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

public class SoundPlayer {

	private MediaPlayer mediaPlayer;
	private Context mContext;
	private MainActivity mainController;
	private OnCompletionListener onComplete = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			mainController.startProcessingDataFromCamera();
		}
	};
	
	public SoundPlayer(Context context, MainActivity mainCtr){
		mContext = context;
		mainController = mainCtr;
		mediaPlayer = MediaPlayer.create(context, ActitivtyConstant.LINK_TO_SOUND_FILE);
		mediaPlayer.setVolume(0.1F, 0.1F);
		mediaPlayer.setOnCompletionListener(onComplete);
	}

	public void start() {
		mediaPlayer.start();		
	}
}
