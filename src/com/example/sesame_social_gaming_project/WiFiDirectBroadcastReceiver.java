package com.example.sesame_social_gaming_project;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;


/**
 * A BroadcastReceiver that notifies of important Wi-Fi p2p events.
 */

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver{

	private ServerAsync serverTask;
	private ClientAsync clientTask;
	private boolean isGroupOwner = false;
	
	private WifiP2pManager mManager;
    private Channel mChannel;
    private MainActivity mActivity;
	protected List<WifiP2pDevice> peerList = new ArrayList<WifiP2pDevice>();

	private WifiListAdapter adapterObserver;
	protected WifiP2pInfo mWifiP2pInfo;
	private String debugTag = "WIFI_DIRECT_BROADCAST";

    public WiFiDirectBroadcastReceiver(WifiP2pManager manager, Channel channel,
    		MainActivity activity) {
        super();
        this.mManager = manager;
        this.mChannel = channel;
        this.mActivity = activity;
    }
    
    //This method will be invoked when there is a change in connection status
    //Using this method to show connection status on UI, or to show list of peers and their status
	@Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.v(debugTag, "action " + action);
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
        	int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
            	//mActivity.setWifiEnabled(true);
            	Log.v(debugTag,"P2P enabled");
            } else {
            	//mActivity.setWifiEnabled(false);
            }
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // Call WifiP2pManager.requestPeers() to get a list of current peers
        	if (mManager != null) {
                requestForPeersList();                
            } else{
            	Log.e(debugTag, "mManager is NULL.");
            }
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // Respond to new connection or disconnections
            if (mManager == null) {
                return;
            }

            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()) {
                // We are connected with the other device, request connection
                // info to find group owner IP
            	mManager.requestConnectionInfo(mChannel, new ConnectionInfoListener() {

					public void onConnectionInfoAvailable(WifiP2pInfo info) {
						Log.v(debugTag, "Successfully connect");
				        /*
				         * The group owner accepts connections using a server socket and then spawns a
				         * client socket for every client. This is handled by {@code
				         * GroupOwnerSocketHandler}
				         */
			        	//mActivity.stsAppendTxt("Connection Info Available");
				        if (info.isGroupOwner && serverTask == null) {
				        	isGroupOwner = true;
				        	serverTask = (new ServerAsync(mActivity,info));
				        	//mActivity.stsAppendTxt("Executing ServerAsync Task");
				        	serverTask.execute();
				        } else if(!info.isGroupOwner && clientTask == null) {
				        	isGroupOwner = false;
				        	clientTask = (new ClientAsync(mActivity,info));
				        	//mActivity.stsAppendTxt("Executing Client Task");
				        	clientTask.execute();
				        }
						
						mWifiP2pInfo = info;
						//mActivity.stsAppendTxt("Is group owner: " + info.isGroupOwner);
						//mActivity.stsAppendTxt("Group owner IP addr: " + info.groupOwnerAddress.getHostAddress());*/
					    //  ", group owner name: "+ info.groupOwnerAddress.getHostName()	
					}
				});
            } else{
            	// It's a disconnect
            }
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
        }
    }

	// to request for peers and show on UI via adapter
	private void requestForPeersList() {
		mManager.requestPeers(mChannel, new PeerListListener() {
			public void onPeersAvailable(WifiP2pDeviceList peers) {
				// if peers are available, update peerList and notify adapter to show on UI						
				peerList.clear();
				peerList.addAll(peers.getDeviceList());
				if(peerList.isEmpty()){
					Log.d(debugTag, "No Peer Found");
				}else{
					Log.v(debugTag, "Devices available");
				}
				if(adapterObserver != null)	adapterObserver.notifyDataSetChanged();
			}
		});
	}
    
    public static String getDeviceStatusStr(int status) {
		switch(status){
		case WifiP2pDevice.CONNECTED:return "Connected";
		case WifiP2pDevice.INVITED:	return "Invited";
		case WifiP2pDevice.FAILED:	return "Failed";
		case WifiP2pDevice.AVAILABLE:	return "Available";
		case WifiP2pDevice.UNAVAILABLE:	return "Unavailable";
		default:	return "Unknown";
		}
	}
    //this method will be invoked when user clicks on an item in peerList
    public OnItemClickListener onPeerClicked = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Log.v(debugTag,"Item clicked, position = " + position);
			WifiP2pDevice device = peerList.get(position);
			if(device == null){
				((WifiListAdapter) parent.getAdapter()).notifyDataSetChanged();
				Log.v(debugTag,"device is null");
				return;	// didn't actually make connection
			} else if(device.status == WifiP2pDevice.CONNECTED){
				// connected already... therefore should do nothing here.
				((WifiListAdapter) parent.getAdapter()).notifyDataSetChanged();
				Log.v(debugTag,"device is alrd connected");
				return;
			}
			// make connection to device
			((TextView)view.findViewById(R.id.wifi_device_status)).setText("");
			Log.v(debugTag,"device is alrd connected");
			connectToPeer(device);
		}
	};
	
	public void connectToPeer(WifiP2pDevice device){
		Log.i(debugTag,"Trying to connect to device name: " + device.deviceName + ", address: " + device.deviceAddress);
		//obtain a peer from the WifiP2pDeviceList
	    WifiP2pConfig config = new WifiP2pConfig();
	    config.deviceAddress = device.deviceAddress;
	    config.wps.setup = WpsInfo.PBC;
	    config.groupOwnerIntent = 15; // set 15 to be the group Owner
	    mManager.connect(mChannel, config, new ActionListener() {
	    
	        public void onSuccess() {
	        	// update status
	        	adapterObserver.notifyDataSetChanged();
	        }

	        public void onFailure(int reason) {
	        	Log.v(debugTag, "Fail connecting to peers: " + reason);
	        	adapterObserver.notifyDataSetChanged();
	        }
	    });
	}
	
	public List<WifiP2pDevice> getPeerList() {
		return peerList;
	}

	public void setObserver(WifiListAdapter wifiListAdapter) {
		adapterObserver = wifiListAdapter;
	}
	
	public String sendMessageToAll(String message){
		if(isGroupOwner && serverTask.isConnected()){//server is ready
			int size = serverTask.sendToAll(message);
			return "Server sent: " + message + " - " + size;
		}
		if(!isGroupOwner && clientTask.isConnected()){//client is ready
			clientTask.sendMsg(message);
			return "Client sent: " + message;
		}
		return ActitivtyConstant.ERROR_MESSAGE;
	}
	/*public boolean connectionSetted() {
		// TODO: return true when connection between two peers is setted
		return serverClientConnectionSet;
	}*/
}
