package com.example.sesame_social_gaming_project;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;

import android.util.Log;

class ServerObject{
	private boolean isSendingVerified = true;
	public boolean isReadingVerified = true;
	
	private ByteBuffer sendBf = ByteBuffer.allocate(ActitivtyConstant.DEFAULT_BUF_SIZE);
	
	public ArrayList<SocketChannel> listOfSocketChannel = new ArrayList<SocketChannel>();
	public ArrayList<Integer> listOfReadSize = new ArrayList<Integer>();
	public ArrayList<ByteBuffer> listOfReadBuffer = new ArrayList<ByteBuffer>();
	public ArrayList<StringBuilder> listOfStringBuilder = new ArrayList<StringBuilder>();
	
	public LinkedBlockingDeque<MessageChannel> sendQueue = new LinkedBlockingDeque<MessageChannel>();
	public LinkedBlockingDeque<MessageChannel> receiveQueue = new LinkedBlockingDeque<MessageChannel>();
	
	private String debugTag = "SERVER";
	protected boolean serverClientConnectionSet = false;

	public ServerObject(boolean isRead) {
		sendBf.flip();
	}
	
	//to send a message 
	private void putMsgToSend(String strToSet) throws IOException{
		if(sendBf.hasRemaining())	throw new IOException("Send Buffer Non-Empty when trying to put in new message");
		sendBf.clear();
		byte[] byetToSet = strToSet.getBytes();
		sendBf.putInt(byetToSet.length);
		sendBf.put(byetToSet);
		sendBf.flip();
	}

	//to read the message when it is available at SocketChannel
	public String readFromChannel(SocketChannel client) throws IOException{
		//there might be many clients, thus need to find the corresponding ByteBuffer, StringBuilder and Size
		int clientIndex = findClientIndex(client);
		ByteBuffer readBf = findByteBuffer(client);
		if(readBf == null){
			Log.v(debugTag, "ERROR: result is null");
			return null;
		}
		
		client.read(readBf);
		readBf.flip();
		String result = null;
		result = exhaustAndCrazyReading(clientIndex);
		readBf.compact();
		return result;
	}

	private ByteBuffer findByteBuffer(SocketChannel client) {
		for(int index = 0; index < listOfReadBuffer.size(); index++){
			if(index >= listOfSocketChannel.size()){
				Log.v(debugTag, "FATAL ERROR: sizes of listOfReadBuffer and listOfChannel are different");
			}
			if(client == listOfSocketChannel.get(index)){
				return listOfReadBuffer.get(index);
			}
		}
		Log.v(debugTag, "ERROR: cant find the correct ReadBuffer");
		return null;
	}

	private int findClientIndex(SocketChannel client) {
		for(int index = 0; index < listOfSocketChannel.size(); index++){
			if(client == listOfSocketChannel.get(index)){
				return index;
			}
		}
		Log.v(debugTag, "ERROR: cant find the correct ReadSize");
		return -1;
	}
	
	private String exhaustReading(ByteBuffer readBf, StringBuilder readSb, Integer readSize) {
		String res = null;
		
		//resultFeatures need to clear all the character which had alr read previously
		for(int tempCount = 0; tempCount < readSize; tempCount++){
			readBf.get();
		}
		
		while(readBf.hasRemaining()){
			Log.v(debugTag, "W1 - Remaining Size: " + readBf.remaining() + ", readsize = " + readSize + " current string: " + readSb.toString());
			readSb.append((char)readBf.get()); 
			readSize++;	// Exhaust the read of last time
		}
		Log.v(debugTag, "R1: Remaining Size: " + readBf.remaining() + ", readSize = " + readSize);
		res = readSb.toString();	// done with a reading
		readSb = null;
		return res;
		
		/*if(readSize == 0 && readBf.remaining() >= 4){
			readSize = readBf.getInt(); readSb = new StringBuilder();
			Log.v(debugTag, "W - Remaining Size: " + readBf.remaining() + ", readSize = " + readSize);
		}
		while(readSize > 0 && readBf.hasRemaining()){
			Log.v(debugTag, "W2 - Remaining Size: " + readBf.remaining() + ", readsize = " + readSize);
			readSb.append((char)readBf.get()); readSize--;	// Exhaust the read of last time
		}
		if(readSize == 0 && readSb != null){
			Log.v(debugTag, "R2: Remaining Size: " + readBf.remaining() + ", readSize = " + readSize);
			res = readSb.toString();	// done with a reading
			readSb = null;
			return res;
		}
		return null;*/
	}
	
	//the socket will read twice: 
	//1st time: always read 4 bytes (potentially overhead/encryption -> so need to ignore these 4 bytes)
	//2nd time: read the data sent to this socketchannel
	private String exhaustAndCrazyReading(int index) {
		String result = null;
		int byteCount = 0;
		Log.v(debugTag, "star reading here " + index);
		
		while(listOfReadBuffer.get(index).hasRemaining()){
			listOfStringBuilder.get(index).append((char)listOfReadBuffer.get(index).get()); 
			byteCount++;	// Exhaust the read of last time
			Log.v(debugTag, "W1 - Remaining Size: " + listOfReadBuffer.get(index).remaining() 
					+ ", readsize = " + byteCount 
					+ " current string: " + listOfStringBuilder.get(index).toString());
		}
		if(byteCount == 4){
			//meaning this is just the overhead, should ignore all the characters and reset Stringbuilder
			listOfStringBuilder.set(index, new StringBuilder());
			return null;
		}
		
		//meaning this is the real message
		Log.v(debugTag, "R1: Remaining Size: " + listOfReadBuffer.get(index).remaining() 
				+ ", readSize = " + byteCount);
		
		listOfReadSize.set(index, byteCount);
		result = listOfStringBuilder.get(index).toString();	// done with a reading
		listOfStringBuilder.set(index, new StringBuilder());
		return result;
	}
	
	public void verifyReading(SocketChannel client) throws IOException {
		String readStr = readFromChannel(client);
		if(readStr == null)	{
			return;
		}
		Log.v(debugTag, "Receive confirm string " + readStr);
		//mActivity.stsAppendTxt("Read Str: " + readStr);
		if(!readStr.equals(ActitivtyConstant.CONFIRM_STRING))	throw new IOException("Read Verify Failed. Got: " + readStr + ", expected: " + ActitivtyConstant.CONFIRM_STRING);
		
		isReadingVerified = true;
		serverClientConnectionSet = true;
		//mActivity.openCvViewSetup();
		//mActivity.initThreadOfExec();
	}
	
	// to broadcast the message in the queue to all client
	public void onWriteReadyMutilpleClients(SocketChannel client) throws IOException {
		/*if(!isSendingVerified){//make sure to verify the sending 
			putMsgToSend(Constant.CONFIRM_STRING);
			isSendingVerified = true;
			client.write(sendBf);
			Log.v(debugTag, "sent out confirm string");
		} else */if(this.isReadingVerified){//make sure to verify the reading
			for(MessageChannel recipient: sendQueue){
				if(recipient.getRecipientChannel() == client && recipient.getStatus() == ActitivtyConstant.MESSAGE_STATUS_IN_QUEUE){
					String sendStr = recipient.getSenderCode()+ ActitivtyConstant.SEPARATOR_CODE + recipient.getMessage();
					if(sendStr == null)	return;	// consider de-register it when there's nothing to send
					
					putMsgToSend(sendStr);
					client.write(sendBf);
					Log.v(debugTag, "sent out to " + recipient.toString() + ": " + sendStr);
					recipient.setStatus(ActitivtyConstant.MESSAGE_STATUS_SENT);
					//break;
				}
			}
		} // else do nothing
	}
	
	public void onReadReady(SocketChannel client) throws IOException {
		//if(isReadingVerified){// only accept the message when reading has been verify
			String readStr = readFromChannel(client);
			String senderCode = findSenderCode(client);
			if(readStr != null && senderCode != null){
				receiveQueue.push(new MessageChannel(client, readStr, senderCode));
				Log.v(debugTag, "Server received from " + senderCode + ": " + readStr);
				return;
			}
	}

	private String findSenderCode(SocketChannel client) {
		for(int index = 0; index < ActitivtyConstant.CLIENT_CODE.length; index++){
			if(client == listOfSocketChannel.get(index)){
				//Log.v(debugTag, "senderCode found " + Constant.CLIENT_CODE[index]);
				return ActitivtyConstant.CLIENT_CODE[index];
			}
		}
		Log.v(debugTag, "fail to find senderCode");
		return null;
	}
}
