package com.example.sesame_social_gaming_project;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import android.net.wifi.p2p.WifiP2pInfo;
import android.os.AsyncTask;
import android.util.Log;

class ServerAsync extends AsyncTask<Void, Void, Void> {
	private MainActivity mAcitity;
	private String debugTag = "SERVER_ASYNC";
	private ServerObject mServer;
	private boolean isConnected = false;

	public ServerAsync(MainActivity activity, WifiP2pInfo info) {
		mAcitity = activity;
		mServer = new ServerObject(true);
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			// open server socket
			ServerSocketChannel serverChannel = ServerSocketChannel.open();
			ServerSocket serverSocket = serverChannel.socket();
			serverSocket.bind(new InetSocketAddress(ActitivtyConstant.PORT_NUM));
			serverChannel.configureBlocking(false);

			// init selector: so that it can manage multiple connection with
			// many clients
			Selector mSelector = Selector.open();
			serverChannel.register(mSelector, SelectionKey.OP_ACCEPT);
			Log.v(debugTag, "Server starts");

			while (true) {
				serverProcessData(mSelector);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private void serverProcessData(Selector mSelector) throws IOException,
			ClosedChannelException {
		mSelector.select();
		Set readKeys = mSelector.selectedKeys();
		Iterator<Set> iterator = readKeys.iterator();

		while (iterator.hasNext()) {
			SelectionKey key = (SelectionKey) iterator.next();
			iterator.remove();
			if (key.isAcceptable()) {// if there is a coming connection request
				addNewClient(mSelector, key);
			} else if (key.isWritable()) {// if server has something to send
				SocketChannel client = (SocketChannel) key.channel();
				ServerObject sobj = (ServerObject) key.attachment();
				if (sobj != mServer) {
					Log.v(debugTag, "ERROR: different server object");
				}
				sobj.onWriteReadyMutilpleClients(client);

			} else if (key.isReadable()) {// if the server receives some
											// incoming data
				receivedDataFromClient(key);
			}
		}
	}

	private void receivedDataFromClient(SelectionKey key) throws IOException {
		SocketChannel client = (SocketChannel) key.channel();
		ServerObject sobj = (ServerObject) key.attachment();
		if (sobj != mServer) {
			Log.v(debugTag, "ERROR: different server object");
		}
		// incoming message will be stored in receiveQueue
		sobj.onReadReady(client);
		if (sobj.receiveQueue.size() > 0) {
			MessageChannel senderMessage = sobj.receiveQueue.pop();
			mAcitity.stsAppendTxt(senderMessage.getSenderCode() + ": "
					+ senderMessage.getMessage());
			relayMessageToAll(senderMessage.getMessage(), client,
					senderMessage.getSenderCode());

		}
	}

	private void addNewClient(Selector mSelector, SelectionKey key)
			throws IOException, ClosedChannelException {
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key
				.channel();
		SocketChannel clientSocketChannel = (SocketChannel) serverSocketChannel
				.accept();
		Log.v(debugTag, "Accept connection from: " + clientSocketChannel);

		// configure non-blocking channel (asynchronous)
		clientSocketChannel.configureBlocking(false);

		SelectionKey key2 = clientSocketChannel.register(mSelector,
				SelectionKey.OP_READ | SelectionKey.OP_WRITE);
		key2.attach(mServer);

		// add a new client and queue to the list
		mServer.listOfSocketChannel.add(clientSocketChannel);
		mServer.listOfReadBuffer.add(ByteBuffer
				.allocate(ActitivtyConstant.DEFAULT_BUF_SIZE));
		mServer.listOfStringBuilder.add(new StringBuilder());
		mServer.listOfReadSize.add(new Integer(0));
		isConnected = true;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public int sendToAll(String message) {
		for (SocketChannel elementSocket : mServer.listOfSocketChannel) {
			mServer.sendQueue.add(new MessageChannel(elementSocket, message,
					ActitivtyConstant.SERVER_CODE));
		}
		return mServer.sendQueue.size();
	}

	public int relayMessageToAll(String message, SocketChannel originalSender,
			String senderCode) {
		for (SocketChannel elementSocket : mServer.listOfSocketChannel) {
			if (elementSocket != originalSender) {
				mServer.sendQueue.add(new MessageChannel(elementSocket,
						message, senderCode));
			}
		}
		return mServer.sendQueue.size();
	}
}
