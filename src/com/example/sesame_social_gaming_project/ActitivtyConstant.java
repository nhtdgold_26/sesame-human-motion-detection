package com.example.sesame_social_gaming_project;

enum ActivityStatus{
	SHOWING_CONNETION_SETTING_DIALOG, SHOWING_CHAT_MESSENGER, SHOWING_CHOOSING_PREVIEW_SIZE, NONE
}
enum Orientation {
	SCREEN_VERTICAL, SCREEN_HORIZONTAL
}
public class ActitivtyConstant {
	//UI Constants
	public static final String DIALOG_TITLE_CONNECTION_DETAILS = "Connection Details";
	public static final String DIALOG_TITLE_CHOOSING_PREVIEW_SIZE_OPENCV = "OpenCV - Choose Preview Size:";
	public static final String DIALOG_TITLE_CHAT_MESSENGER = "Chat Messengers";
	public static final String DIALOG_TITLE_CONFIRM = "Are you sure?";
	
	public static final String BUTTON_POSITIVE_ACTION = "Ok";
	public static final String BUTTON_NEGATIVE_ACTION = "Cancel";
	
	public static final String ERROR_MESSAGE = "Sending message failed";
	public static final String CONFIRM_STRING = "confirm";
	public static final String SEPARATOR_CODE = "/";
	
	//Wifi Direct constants
	public static final int DEFAULT_BUF_SIZE = 1000;
	public static final int MAX_NUMBER_OF_CLIENTS = 10;
	public static final int PORT_NUM = 9000;
	public static final int MESSAGE_STATUS_IN_QUEUE = 0;
	public static final int MESSAGE_STATUS_SENT = 1;
	public static final long TIME_POLLING_INTERVAL = 20;
	public static final int FONT_CAMERA_ID = 1;
	
	public static final String COMMAND_INDICATOR = "***";
	public static final String COMMAND_START_CAPTURING = "START_CAPTURING";
	public static final String COMMAND_STOP_CAPTURING = "STOP_CAPTURING";
	
	//Chat messenger constants
	public static final String SERVER_CODE = "The Sun";
	public static final String[] CLIENT_CODE = 
		{"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"};
	
	//Sound player
	public static final int LINK_TO_SOUND_FILE = R.raw.countdown_10;
}
