package com.example.sesame_social_gaming_project;

public interface PauseResumeListener {
	public void onPause();
	public void onResume();
}