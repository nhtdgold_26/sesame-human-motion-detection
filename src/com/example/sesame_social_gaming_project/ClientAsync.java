package com.example.sesame_social_gaming_project;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import android.net.wifi.p2p.WifiP2pInfo;
import android.os.AsyncTask;
import android.util.Log;


class ClientAsync extends AsyncTask<Void, Void, Void>{
	private MainActivity mAcitity;
	WifiP2pInfo info;
	private String debugTag = "CLIENT_ASYNC";
	public LinkedBlockingDeque<String> sendQueue = new LinkedBlockingDeque<String>();
	public LinkedBlockingDeque<String> revQUeue = new LinkedBlockingDeque<String>();
	public ArrayList<MessageQueueToMultiplePeers> sendQueueMultipleClients = new ArrayList<MessageQueueToMultiplePeers>();
	private boolean isConnected = false;
	private TimerTask readingFromStream;
	private TimerTask sendingToStream;
	private Timer timerReading = new Timer();
	private Timer timerSending = new Timer();
	
	public ClientAsync(MainActivity activity,WifiP2pInfo info) {
		this.mAcitity = activity;
		this.info = info;
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			Socket socket = null;
			do{
				try{
					socket = new Socket(info.groupOwnerAddress,ActitivtyConstant.PORT_NUM);
				} catch(IOException e){
					Log.v(debugTag, "Trying to make connection failed");
					TimeUnit.MICROSECONDS.sleep(200);
				};
			} while(socket == null);
			
			Log.v(debugTag, "Client starts, group owner, whose addr is: " + info.groupOwnerAddress);
			DataInputStream dis = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
			DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
			//String rstr = readFromDIS(dis);			
			//Log.v(debugTag, "Received confirm string: " + rstr);
			//writeToDOS(dos, Constant.CONFIRM_STRING);
			//checkStrEqual(rstr, Constant.CONFIRM_STRING);
			startCommnunicationWithServer(dis,dos);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//to start reading and writing message to server when info is ready in the queue
	public void startCommnunicationWithServer(final DataInputStream dis, final DataOutputStream dos) {
		
		// start a thread to read message
		isConnected = true;
		readingFromStream = new TimerTask() {       
	        @Override
	        public void run() {
				try{
					String receivedMessage = readFromDIS(dis);
						
					//from the server message, identify who the sender is by senderCode
					String senderCode = findSenderCode(receivedMessage);
					String realMessage = findRealMessage(receivedMessage);
						
					if(receivedMessage == null || senderCode == null || realMessage == null){
						return;
					}
						
					revQUeue.push(realMessage);
					mAcitity.stsAppendTxt(senderCode + ": " + realMessage);
					Log.e(debugTag, "Received from " + senderCode + ": " + realMessage);
				} catch(IOException e){
					Log.e(debugTag, "markConnectionSet: IO Exception Caught");
					e.printStackTrace();
				}
	        }
		};
		
		timerReading.scheduleAtFixedRate(readingFromStream, ActitivtyConstant.TIME_POLLING_INTERVAL, ActitivtyConstant.TIME_POLLING_INTERVAL);
		
		sendingToStream = new TimerTask() {       
	        @Override
	        public void run() {
	        	//keep checking from queue for info, write to socket if info is ready
				String strToSend = null;
				try {
					strToSend = sendQueue.poll(ActitivtyConstant.TIME_POLLING_INTERVAL ,TimeUnit.MILLISECONDS);
					if(strToSend != null) 
					{
						//only can send to server
						writeToDOS(dos, strToSend);
						Log.v("MESSAGE", "message sent to server: " + strToSend);	
					}else{
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }
		};
		
		timerSending.scheduleAtFixedRate(sendingToStream, ActitivtyConstant.TIME_POLLING_INTERVAL, ActitivtyConstant.TIME_POLLING_INTERVAL);
	}
	
	// to find senderCode from the message received
	protected String findSenderCode(String receivedMessage) {
		if(receivedMessage == null || receivedMessage.length() == 0){
			return null;
		}
		//look for SEPARATOR
		int separatorIndex = receivedMessage.indexOf(ActitivtyConstant.SEPARATOR_CODE);
		String senderCode = receivedMessage.substring(0, separatorIndex);
		return senderCode;
	}

	// to attract the real message from the message received
	protected String findRealMessage(String receivedMessage) {
		if(receivedMessage == null || receivedMessage.length() == 0){
			return null;
		}
		//look for SEPARATOR
		int separatorIndex = receivedMessage.indexOf(ActitivtyConstant.SEPARATOR_CODE);
		String realMessage = receivedMessage.substring(separatorIndex + 1);
		return realMessage;
	}
	
	public boolean sendMsg(String msg){
		try {
			// get message from queue
			sendQueue.put(msg);
			Log.v(debugTag, "put into queue " + msg);
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	//broadcast message to all clients
	/*public boolean sendMsgBroadcast(String cmd,String msg){
		try {
			if(isGroupOwner){
				for(MessageQueueToMultiplePeers objMsgQueue : sendQueueMultipleClients){
					objMsgQueue.put(cmd + " " + msg);
					Log.v("MESSAGE", "message broadcast sent: " + cmd);
				}
			}
			else
				sendQueue.put(cmd + " " + msg);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}*/
	
	// to read message from inputStream
	private static String readFromDIS(DataInputStream dis) throws IOException {
		int strlength = dis.readInt();
		Log.v("DEBUG", "length = " + strlength);
		byte[] strs = new byte[strlength];
		dis.readFully(strs);
		return new String(strs);
	}
	
	//to write to outputStream
	private static void writeToDOS(DataOutputStream dos,String str) throws IOException{
		// write a string, with fixed given size
		byte[] sbuf = str.getBytes();
		dos.writeInt(sbuf.length); dos.flush();
		dos.write(sbuf); dos.flush();
	}
	
	private static void checkStrEqual(String a, String b) throws IOException{
		if(!a.equals(b))	throw new IOException("Confirm Str mismatch. a = " + a + ", b = " + b);
	}
	
	public boolean isConnected(){
		return isConnected;
	}
}
