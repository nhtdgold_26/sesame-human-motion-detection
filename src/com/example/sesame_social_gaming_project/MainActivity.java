package com.example.sesame_social_gaming_project;

import java.util.List;

import org.opencv.android.CameraBridgeViewBase;

import sesame.HumanAction;
import sesame.actiondetection.SinglePhoneActionDetection;
import sesame.actiondetection.SinglePhoneHumanMotionListener;
import ApplicationHelper.ApplicationHelper;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Point;
import android.hardware.Camera;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sesame_social_gaming_project.util.SystemUiHider;
/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class MainActivity extends MVCActivityTemplate {
	
	//self create object
	private WifiP2pManager mManager;
	private Channel mChannel;
	private WiFiDirectBroadcastReceiver mReceiver;
	private IntentFilter mIntentFilter;
	private Button btnConnectionSettings;
	private Button btnChatMessenger;
	private Button btnCameraSettings;
	private CheckBox cameraCheckboxOpenCV;
	private CheckBox checkboxProcessFullBody;
	private CheckBox showResultCheckbox;
	private CheckBox showROICheckbox;
	private CheckBox showPointsCheckbox;
	private CheckBox showFullImageCheckbox;
	private View controlsView;
	private View contentView;
	private ListView mListView;
	private TextView chatMessengerTextView;
	private ActivityStatus activityStatus = ActivityStatus.NONE;
	private String debugTag = "MAIN_ACTIVITY";
	private SinglePhoneActionDetection singlePhoneActionDetector;
	private SoundPlayer mMediaPlayer;
	private CameraBridgeViewBase mOpenCvCameraView;
	private Orientation screenOrientation = Orientation.SCREEN_VERTICAL;
	private List<android.hardware.Camera.Size> listOfPreviewSizes;
	//end self create object
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_human__motion);

		controlsView = findViewById(R.id.fullscreen_content_controls);
		
		//set up UI components (buttons, checkbox, etc.)
		btnConnectionSettings = (Button)findViewById(R.id.connectionManagementButton);
		btnChatMessenger = (Button)findViewById(R.id.chatManagementButton);
		btnCameraSettings = (Button)findViewById(R.id.cameraSettingsButton);
		mListView = new ListView(getApplicationContext());
		chatMessengerTextView = (TextView) findViewById(R.id.chatMessengerTextView);
		
		btnConnectionSettings.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				showDialogConnectionSettings();
				
			}
		});
		
		btnChatMessenger.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				setUpChatMessengerUI();
			}
		});
		
		findViewById(R.id.sendButton).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				sendMessageToAll();				
			}
		});
		
		cameraCheckboxOpenCV = (CheckBox)findViewById(R.id.checkboxOpenCV);
		cameraCheckboxOpenCV.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(cameraCheckboxOpenCV.isChecked()){
					singlePhoneActionDetector.stopCapturing();
					showDialogOpenCVChoosePreviewSize();
					cameraCheckboxOpenCV.setChecked(true);
				}else{
					singlePhoneActionDetector.stopCapturing();
					cameraCheckboxOpenCV.setChecked(false);
				}	
			}
		});
		
		checkboxProcessFullBody = (CheckBox)findViewById(R.id.checkboxProcessFullBody);
		checkboxProcessFullBody.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(checkboxProcessFullBody.isChecked()){
					checkboxProcessFullBody.setChecked(true);
					singlePhoneActionDetector.setProcessFullBody(true);
				}else{
					singlePhoneActionDetector.setProcessFullBody(false);
					checkboxProcessFullBody.setChecked(false);
				}	
			}
		});
		showResultCheckbox = (CheckBox)findViewById(R.id.checkboxShowClusterResult);
		showResultCheckbox.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(showResultCheckbox.isChecked()){
					singlePhoneActionDetector.setShowResultSkeleton(true);
					showResultCheckbox.setChecked(true);
				}else{
					singlePhoneActionDetector.setShowResultSkeleton(false);
					showResultCheckbox.setChecked(false);
				}
			}
		});
		
		showROICheckbox = (CheckBox)findViewById(R.id.checkboxShowROI);
		showROICheckbox.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(showROICheckbox.isChecked()){
					singlePhoneActionDetector.setShowROI(true);
					showROICheckbox.setChecked(true);
				}else{
					singlePhoneActionDetector.setShowROI(false);
					showROICheckbox.setChecked(false);
				}
			}
		});
		
		showPointsCheckbox = (CheckBox)findViewById(R.id.checkboxShowPoints);
		showPointsCheckbox.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(showPointsCheckbox.isChecked()){
					singlePhoneActionDetector.setShowPoints(true);
					showPointsCheckbox.setChecked(true);
				}else{
					singlePhoneActionDetector.setShowPoints(false);
					showPointsCheckbox.setChecked(false);
				}
			}
		});
		
		showFullImageCheckbox = (CheckBox)findViewById(R.id.checkboxShowFullImage);
		showFullImageCheckbox.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(showFullImageCheckbox.isChecked()){
					singlePhoneActionDetector.setShowFullImage(true);
					showFullImageCheckbox.setChecked(true);
				}else{
					singlePhoneActionDetector.setShowFullImage(false);
					showFullImageCheckbox.setChecked(false);
				}
			}
		});
		
		//setup main components
		cameraManagerInit();
		soundPlayerInit();
		//this part belongs to networking set-up 
		wifiDirectInit();
	}	
	
	//this function is only used together with network synchronization
	public void startProcessingDataFromCamera(){
		singlePhoneActionDetector.startProcessFrame();
	}
	
	protected void showDialogOpenCVChoosePreviewSize() {
		activityStatus = ActivityStatus.SHOWING_CHOOSING_PREVIEW_SIZE;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(ActitivtyConstant.DIALOG_TITLE_CHOOSING_PREVIEW_SIZE_OPENCV + toCharSequence(listOfPreviewSizes.toArray(), listOfPreviewSizes.size()).length);
		builder.setCancelable(false);
		
		builder.setItems(toCharSequence(listOfPreviewSizes.toArray(), listOfPreviewSizes.size()), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int index) {
				activityStatus = ActivityStatus.NONE;
				singlePhoneActionDetector.setPreviewSize(listOfPreviewSizes.get(index));
				startCapturing();				
				mMediaPlayer.start();
			}
		});
		
		//change the parameter of the dialog shown on the screen. just for UI purpose	
		Dialog dialog = builder.show();
		WindowManager.LayoutParams dialogParams = new WindowManager.LayoutParams();
		dialogParams.alpha = (float)0.9;
		dialogParams.height = getScreenSize().y * 3/4;
		dialog.getWindow().setAttributes(dialogParams);	
	}
	
	protected void startCapturing() {
		singlePhoneActionDetector.startCapturing();		
	}
	
	private CharSequence[] toCharSequence(Object[] inputArray, int size) {
		CharSequence[] result = new CharSequence[size];
	
		for(int index = 0; index < size; index++){
			result[index] = ((android.hardware.Camera.Size)inputArray[index]).height + 
							"x" + 
							((android.hardware.Camera.Size)inputArray[index]).width;
		}
		return result;
	}

	private void cameraManagerInit() {
		mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.cam_view);
		
		//this SinglePhoneActionDetection object is the only link to the whole API behind
		singlePhoneActionDetector = new SinglePhoneActionDetection(this.getApplicationContext(),
				new SinglePhoneHumanMotionListener() {
					
					public void onActionDetected(HumanAction detectedAction) {
						// TODO Auto-generated method stub
						ApplicationHelper.showToastMessage("ACTION DETECTED " + detectedAction);
					}
				}, mOpenCvCameraView);
		
		//CameraBridgeViewBase UI component needs to be linked with a CvCameraViewListener 
		mOpenCvCameraView.setCvCameraViewListener(singlePhoneActionDetector.getCameraManager());
		
		//get the list of preview size to show to users later
		//choosing high res preview size will affect the performance of the app
		//generally we choose 320x240 or equivalent 
		listOfPreviewSizes = singlePhoneActionDetector.getListOfPreviewSize();
	}
	
	//this function is only used together with network synchronization
	private void soundPlayerInit() {
		mMediaPlayer = new SoundPlayer(this.getApplicationContext(), this);
	}

	//this function is only used together with network
	private void sendMessageToAll(){
		String messageToSend = ((EditText)findViewById(R.id.editTextMessageToSend)).getText().toString();
		if(messageToSend == null || messageToSend.length() == 0){
			Log.v(debugTag, "empty message to send");
			return;
		}
		Log.v(debugTag, "" + mReceiver.sendMessageToAll(messageToSend));
		stsAppendTxt("Me: " + messageToSend);
	}
	
	//this function is only used together with network
	public void stsAppendTxt(final String textToDisplay) {
		runOnUiThread(new Runnable() {
			public void run() {
				chatMessengerTextView.append("\n");
				if (chatMessengerTextView != null) {
					chatMessengerTextView.append(textToDisplay);
				}
			}
		});
	}
	
	//this function is only used together with network
	protected void setUpChatMessengerUI() {
		activityStatus = ActivityStatus.SHOWING_CHAT_MESSENGER;
		findViewById(R.id.sendButton).setVisibility(View.VISIBLE);
		findViewById(R.id.editTextMessageToSend).setVisibility(View.VISIBLE);
		findViewById(R.id.checkboxOpenCV).setVisibility(View.GONE);
		findViewById(R.id.checkboxProcessFullBody).setVisibility(View.GONE);
		findViewById(R.id.checkboxShowClusterResult).setVisibility(View.GONE);
		mOpenCvCameraView.setVisibility(View.GONE);
		if(cameraCheckboxOpenCV.isChecked() || checkboxProcessFullBody.isChecked()){
			singlePhoneActionDetector.stopCapturing();
			cameraCheckboxOpenCV.setChecked(false);
			checkboxProcessFullBody.setChecked(false);
		}
		chatMessengerTextView.setVisibility(View.VISIBLE);
		btnCameraSettings.setVisibility(View.GONE);
		btnChatMessenger.setVisibility(View.GONE);
		btnConnectionSettings.setVisibility(View.GONE);		
	}
	
	//this function will reset UI display when users presses BACK button
	protected void setUpMainUI() {
		activityStatus = ActivityStatus.NONE;
		findViewById(R.id.sendButton).setVisibility(View.GONE);
		findViewById(R.id.editTextMessageToSend).setVisibility(View.GONE);
		findViewById(R.id.checkboxOpenCV).setVisibility(View.VISIBLE);
		findViewById(R.id.checkboxProcessFullBody).setVisibility(View.VISIBLE);
		findViewById(R.id.checkboxShowClusterResult).setVisibility(View.VISIBLE);
		mOpenCvCameraView.setVisibility(View.VISIBLE);
		cameraCheckboxOpenCV.setChecked(false);
		chatMessengerTextView.setVisibility(View.GONE);
	}

	//this function is only used together with network
	//it will open a dialog and show all the available peers and their connection status
	protected void showDialogConnectionSettings() {
        activityStatus = ActivityStatus.SHOWING_CONNETION_SETTING_DIALOG;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(ActitivtyConstant.DIALOG_TITLE_CONNECTION_DETAILS);
		builder.setCancelable(false);
		
		//add the list of devices together with their connection status
		//this list will auto update when the connection status changes.
		builder.setView(mListView).
				setPositiveButton(ActitivtyConstant.BUTTON_POSITIVE_ACTION, new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
            	   //to prevent error, must removeView() after the dialog is closed
                   ((ViewGroup)mListView.getParent()).removeView(mListView);
                   activityStatus = ActivityStatus.NONE;
               }
           });
		
		//change the parameter of the dialog shown on the screen		
		Dialog dialog = builder.show();
		WindowManager.LayoutParams dialogParams = new WindowManager.LayoutParams();
		dialogParams.alpha = (float)0.9;
		dialogParams.height = getScreenSize().y * 3/4;
		dialog.getWindow().setAttributes(dialogParams);	
	}
	
	//this function will ask user to confirm quit when user presses BACK button
	private void showDialogConfirmQuit(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(ActitivtyConstant.DIALOG_TITLE_CONFIRM);
		
		builder.setPositiveButton(ActitivtyConstant.BUTTON_POSITIVE_ACTION, new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
            	   finish();
               }
           }).setNegativeButton(ActitivtyConstant.BUTTON_NEGATIVE_ACTION, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
		
		//change the parameter of the dialog shown on the screen		
		Dialog dialog = builder.show();
		WindowManager.LayoutParams dialogParams = new WindowManager.LayoutParams();
		dialogParams.alpha = (float)0.9;
		dialogParams.height = getScreenSize().y * 3/4;
		dialog.getWindow().setAttributes(dialogParams);	
	}
	
	//NOT IMPORTANT. because we fix the ORIENTATION to be always HORIZONTAL
	private Orientation getScreenOrientation() {
		Configuration newConfig = getResources().getConfiguration();
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			//mainLayout.setOrientation(LinearLayout.HORIZONTAL);
			//ratioXScreen = 0.5;
			//ratioYScreen = 1;
			return Orientation.SCREEN_HORIZONTAL;
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			//mainLayout.setOrientation(LinearLayout.VERTICAL);
			//ratioXScreen = 1;
			//ratioYScreen = 0.5;
			return Orientation.SCREEN_VERTICAL;
		}
		return Orientation.SCREEN_VERTICAL;
	}
	//this function is only used together with network
	//this method is to initialize the WifiDirect components 
		private void wifiDirectInit() {
			mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
			mChannel = mManager.initialize(this, getMainLooper(), null);

			mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);

			mIntentFilter = new IntentFilter();
			mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
			mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
			mIntentFilter
					.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
			mIntentFilter
					.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
			
			if(mListView == null){
				return;
			}
			mListView.setAdapter(new WifiListAdapter(getApplicationContext(),
					mReceiver));
			mListView.setPadding(10, 10, 10, 10);
			mListView.setOnItemClickListener(mReceiver.onPeerClicked);
			
			//immediately discover peers around after initialization
			toDiscoverPeers();
		}

		//this function is only used together with network
		//this method is to send a request to look for peers around
		//when there are peers around, the WifiDirectBroadcastReceiver object will handle 
		private void toDiscoverPeers() {
			mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
				public void onSuccess() {
					Log.v("DISCOVER_PEER", "Peers Discovery Succeeded");
				}

				public void onFailure(int reasonCode) {
					Log.v("DISCOVER_PEER", "Peers Discovery Failed");
				}
			});
		}
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
	}
	@SuppressLint("NewApi")
	private Point getScreenSize() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		return size;
	}
			
	@Override
	protected void onResume() {
		Log.v("SYSTEM RESUME", "System Resume");
		super.onResume();
		screenOrientation = getScreenOrientation();
		registerReceiver(mReceiver, mIntentFilter);
	}

	public void setCameraDisplayOrientation() {
		 Camera mCamera = Camera.open(1); // the front camera

	     android.hardware.Camera.CameraInfo info =
	             new android.hardware.Camera.CameraInfo();
	     android.hardware.Camera.getCameraInfo(0, info);
	     int rotation = this.getWindowManager().getDefaultDisplay()
	             .getRotation();
	     int degrees = 0;
	     
	     switch (rotation) {
	         case Surface.ROTATION_0: degrees = 0; break;
	         case Surface.ROTATION_90: degrees = 90; break;
	         case Surface.ROTATION_180: degrees = 180; break;
	         case Surface.ROTATION_270: degrees = 270; break;
	     }

	     int result;
	     if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
	         result = (info.orientation + degrees) % 360;
	         result = (360 - result) % 360;  // compensate the mirror
		     Log.v(debugTag, "Front Orientation changed: " + result);
	     } else {  // back-facing
	         result = (info.orientation - degrees + 360) % 360;
		     Log.v(debugTag, "Back Orientation changed: " + result);
	     }
	     Log.v(debugTag, "Orientation changed: " + result);
	     mCamera.setDisplayOrientation(result);
	     //mOpenCvCameraView.setRotation(result);
	     mCamera.release();
	 }
	
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Checks the orientation of the screen
		screenOrientation = getScreenOrientation();
		singlePhoneActionDetector.stopCapturing();
		setCameraDisplayOrientation();
/*		Log.v(debugTag, "Orientation changed: " + screenOrientation);
		//if(screenOrientation == Orientation.SCREEN_VERTICAL){
			mOpenCvCameraView.setPivotX(0);
			mOpenCvCameraView.setPivotY(0);
			mOpenCvCameraView.setRotation((float)10);*/
			//mOpenCvCameraView.setRotationY((float) 10);
		//}
		//singlePhoneActionDetector.setScreenOrientation(screenOrientation);
		singlePhoneActionDetector.startCapturing();
	}
	
	@Override
    public void onBackPressed() {
		Log.v(debugTag, "Back button is press, status is " + activityStatus);
		switch (activityStatus) {
		case SHOWING_CHAT_MESSENGER:
			setUpMainUI();
			break;
		case NONE: 
			showDialogConfirmQuit();
			break;
		}		
		Log.v(debugTag, "Back button is press, new status is " + activityStatus);
     }
	
	 @Override
	    public void onPause() {
	        super.onPause();
	        unregisterReceiver(mReceiver);
	    }
}
