package com.example.sesame_social_gaming_project;

import java.util.List;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

    public class WifiListAdapter extends BaseAdapter implements ListAdapter{
    	List<WifiP2pDevice> peerList;
    	Context mContext;

		public WifiListAdapter(Context applicationContext,WiFiDirectBroadcastReceiver r) {
			mContext = applicationContext;
			r.setObserver(this);
			peerList = r.getPeerList();
		}

		public int getCount() {
			return peerList.size();
		}

		public Object getItem(int position) {
			return peerList.get(position);
		}

		public long getItemId(int position) {
			Log.v("DEBUG","in getItemId, position = " + position);
			 return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView == null ? View.inflate(mContext, R.layout.connection_list_adapter, null): convertView;
			Log.v("GET_VIEW","in getView, position = " + position);
			view.setPadding(10, 10, 10, 10);
			WifiP2pDevice device = (WifiP2pDevice)getItem(position);
			TextView tvName = (TextView)view.findViewById(R.id.wifi_device_name);
			TextView tvAddr = (TextView)view.findViewById(R.id.wifi_device_addr);
			TextView tvSts = (TextView)view.findViewById(R.id.wifi_device_status);
			tvName.setText(device.deviceName);
			tvAddr.setText(device.deviceAddress);
			tvSts.setText(WiFiDirectBroadcastReceiver.getDeviceStatusStr(device.status));
			return view;
		}
    }

