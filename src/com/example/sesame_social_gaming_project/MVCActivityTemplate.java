package com.example.sesame_social_gaming_project;

import java.util.ArrayList;
import java.util.List;

import ApplicationHelper.ApplicationHelper;
import Controller.Controller;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

abstract public class MVCActivityTemplate extends Activity{
	protected Controller controller;
	
	private List<PauseResumeListener> listeners = new ArrayList<PauseResumeListener>();
	protected boolean performConnectionDiscovery(){return true;}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		ApplicationHelper.setInstance(this);
		
		controller = new Controller(this);
		registerPauseResumeListener(controller);
		controller.performConnectionDiscovery = performConnectionDiscovery();
		
		if(!controller.isCompatible()){
			ApplicationHelper.finishWithMessage("None-Compatible","Sorry, your phone is not compatible with this program.");
		}
	}
	
	protected void registerPauseResumeListener(PauseResumeListener listener){
		this.listeners.add(listener);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		for(PauseResumeListener listener:listeners)	listener.onResume();
		ApplicationHelper.setInstance(this);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		for(PauseResumeListener listener:listeners)	listener.onPause();
	}
}