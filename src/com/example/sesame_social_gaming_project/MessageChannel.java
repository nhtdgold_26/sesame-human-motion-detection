package com.example.sesame_social_gaming_project;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class MessageChannel{
	private String message;
	private String senderCode;
	private SocketChannel recipientChannel;
	private ByteBuffer readBf;
	private int status = ActitivtyConstant.MESSAGE_STATUS_IN_QUEUE;
	
	public MessageChannel(SocketChannel recipient, String msg, String code){
		recipientChannel = recipient;
		message = msg;
		senderCode = code;
		readBf = ByteBuffer.allocate(ActitivtyConstant.DEFAULT_BUF_SIZE);
	}
	
	public boolean setRecipient(SocketChannel recipient){
		try{
			recipientChannel = recipient;
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean setMessage(String msg){
		try{
			message = msg;
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public void setSenderCode(String code){
		senderCode = code;
	}
	
	public void setByteBuffer(ByteBuffer buffer){
		readBf = buffer;
	}

	public void setStatus(int s){
		status = s;
	}
	
	public SocketChannel getRecipientChannel(){
		return recipientChannel;
	}
	
	public String getMessage(){
		return message;
	}
	
	public String getSenderCode(){
		return senderCode;
	}
	
	public ByteBuffer getByteBuffer(){
		return readBf;
	}
	
	public int getStatus(){
		return status;
	}
}
