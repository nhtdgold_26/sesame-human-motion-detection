package com.example.sesame_social_gaming_project;

import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class MessageQueueToMultiplePeers{
	private LinkedBlockingDeque<String> sendMessageQueue;
	private LinkedBlockingDeque<String> receiveMessageQueue;
	private SocketChannel recipientChannel;
	
	public MessageQueueToMultiplePeers(){
		sendMessageQueue = new LinkedBlockingDeque<String>();
		receiveMessageQueue = new LinkedBlockingDeque<String>();
		recipientChannel = null;
	}
	public MessageQueueToMultiplePeers(SocketChannel recipient, LinkedBlockingDeque<String> sendMsgQueue,
			LinkedBlockingDeque<String> receiveMsgQueue){
		recipientChannel = recipient;
		sendMessageQueue = sendMsgQueue;
		receiveMessageQueue = receiveMsgQueue;
	}
	
	public boolean setRecipient(SocketChannel recipient){
		try{
			recipientChannel = recipient;
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean setSendMsgQueue(LinkedBlockingDeque<String> msgQueue){
		try{
			sendMessageQueue = msgQueue;
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean setReceiveMsgQueue(LinkedBlockingDeque<String> msgQueue){
		try{
			receiveMessageQueue = msgQueue;
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public SocketChannel getRecipientChannel(){
		return recipientChannel;
	}
	
	public LinkedBlockingDeque<String> getSendMsgQueue(){
		return sendMessageQueue;
	}
	
	public LinkedBlockingDeque<String> getReceiveMsgQueue(){
		return receiveMessageQueue;
	}
	
	public boolean put(String msg){
		try {
			sendMessageQueue.put(msg);
			return true;
		} catch (InterruptedException e) {
			return false;
		}
	}
	
	public String poll(long timeout, TimeUnit unit){
		try {
			return receiveMessageQueue.poll(timeout, unit);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	public String poll(){
		return receiveMessageQueue.poll();
	}
}
