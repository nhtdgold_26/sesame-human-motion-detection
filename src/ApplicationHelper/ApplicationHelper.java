package ApplicationHelper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.wifi.p2p.WifiP2pDevice;
import android.widget.Toast;

import com.example.sesame_social_gaming_project.MVCActivityTemplate;

public class ApplicationHelper {
	public static int LOGIN_INDENT_CODE = 1;
    

	private static MVCActivityTemplate appInstance;
    
	public static void finishWithMessage(String title,String message) {
    	AlertDialog.Builder alert = new AlertDialog.Builder(appInstance);
    	alert.setTitle(title);
    	alert.setMessage(message);
    	DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				appInstance.finish();
			}
		};
		alert.setPositiveButton("OK", clickListener);
    	alert.show();
	}
    

	public static void showToastMessage(final String message) {
		getActivityInstance().runOnUiThread(new Runnable() {
			@Override public void run() {
				Toast.makeText(appInstance, message, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public static String getDeviceStatusStr(int status) {
		switch(status){
		case WifiP2pDevice.CONNECTED:return "Connected";
		case WifiP2pDevice.INVITED:	return "Invited";
		case WifiP2pDevice.FAILED:	return "Failed";
		case WifiP2pDevice.AVAILABLE:	return "Available";
		case WifiP2pDevice.UNAVAILABLE:	return "Unavailable";
		default:	return "Unknown";
		}
	}

	public static void setInstance(MVCActivityTemplate activityInstance) {
		appInstance = activityInstance;
	}
	
	public static MVCActivityTemplate getActivityInstance(){
		return appInstance;
	}
}