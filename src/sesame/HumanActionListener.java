package sesame;
import java.util.List;


public interface HumanActionListener {
	public List<HumanAction> listenToAction();
	public void onHumanAction(List<HumanAction> actionList);
}
