package sesame.helper;

public abstract class Callbackable {
	private Runnable completionCallback;
	abstract protected void run();
	
	public void start(){
		run();
		complete();
	}
	
	public void onComplete(Runnable completionCallback){
		this.completionCallback = completionCallback;
	}
	

	private void complete() {
		if(completionCallback != null) completionCallback.run();
		completionCallback = null;
	}
}
