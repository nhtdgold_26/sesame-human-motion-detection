package sesame.actiondetection;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import sesame.HumanAction;
import sesame.actiondetection.HumanFrameFactoryGrid.Grid;
import sg.edu.nus.sesamemultiphone.motioncapture.HumanFrame;
import sg.edu.nus.sesamemultiphone.motioncapture.HumanTrajectory;
import sg.edu.nus.sesamemultiphone.motioncapture.NodeType;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.util.Log;

public class CameraViewManager implements CvCameraViewListener {
	public Context appContext;
	private String debugTag = "CAMERA_MANAGER";
	private BaseLoaderCallback mLoaderCallback;
	private CameraBridgeViewBase mCameraView;
	private Mat currentDisplayMat;
	private FaceDetector faceDetector;
	private ArrayList<Mat> listOfImageMatrix;
	private Timer timerProcessFrame = new Timer();
	//private Timer timerDetectMotion = new Timer();
	private TimerTask processFrameTask;
	//private TimerTask detectMotionTask;
	private Rect faceRectange;
	private Point hipPosition;
	private int frameHeight = 0;
	private Size screenSize;
	private HumanNodeDetectorGrid mainHumanDetectorGrid;
	private HumanFrame mainHumanFrame;
	private HumanTrajectory mainHumanTrajectory;
	SinglePhoneHumanMotionListener singlePhoneActionListener;
	private boolean isMovementSignificant = false;
	private boolean showROI = false;
	private boolean showGoodFeatures = false;
	private boolean showFullImage = true;
	private boolean showSkeleton = true;
	private HumanAction latestAction = HumanAction.None;
	private int textCounter = 0;

	public CameraViewManager(Context context, CameraBridgeViewBase view,
			SinglePhoneHumanMotionListener singlePhoneActionListener) {
		appContext = context;
		mCameraView = view;
		listOfImageMatrix = new ArrayList<Mat>();
		mainHumanDetectorGrid = new HumanNodeDetectorGrid();
		this.singlePhoneActionListener = singlePhoneActionListener;
		setupBaseLoaderCallBack();
	}

	public void startCapturing() {
		Log.v(debugTag, "Load now");
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_5, appContext,
				mLoaderCallback);
		Log.v(debugTag, "Load done");
	}

	public void stopCapturing() {
		Log.v(debugTag, "Camera stops");
		mCameraView.disableView();
	}

	private void setupBaseLoaderCallBack() {
		mLoaderCallback = new BaseLoaderCallback(appContext) {
			@Override
			public void onManagerConnected(int status) {
				switch (status) {
				case LoaderCallbackInterface.SUCCESS: {
					Log.v(debugTag, "Camera starts");
					mCameraView.enableView();
				}
					break;
				default: {
					super.onManagerConnected(status);
				}
					break;
				}
			}
		};
	}

	public void onCameraViewStarted(int width, int height) {
		currentDisplayMat = new Mat(height, width, CvType.CV_8UC3);
		listOfImageMatrix = new ArrayList<Mat>();
		faceDetector = new FaceDetector();
		mainHumanDetectorGrid = new HumanNodeDetectorGrid();
		mainHumanTrajectory = new HumanTrajectory();
		faceDetector.setUpFaceDetector(appContext,
				(float) (frameHeight * 1.0 / Constant.MIN_FACE_RATIO),
				(float) (frameHeight * 1.0 / Constant.MAX_FACE_RATIO));

		startThreadToProcessFrame();
		//this is only used with SVM part
		//startThreadToDetectMotion();
		Log.v(debugTag, "Camera starts " + width + " " + height);
	}

	public Mat onCameraFrame(Mat inputFrame) {
		// whenever a new frame comes, it will be add to a stack of frame to be
		// processed later
		// this frame will be add to background removal and face detection
		Mat flippedFrame = new Mat();
		Core.flip(inputFrame, flippedFrame, 1);

		listOfImageMatrix.add(flippedFrame);
		faceDetector.addMatToProcess(flippedFrame);
		faceRectange = faceDetector.getCorrectFace();
		mainHumanFrame = mainHumanDetectorGrid.getHumanFrame();
		try {
			hipPosition = new Point(
					mainHumanFrame.getNode(NodeType.HIP).getX(), mainHumanFrame
							.getNode(NodeType.HIP).getY());
		} catch (Exception e) {
			hipPosition = null;
		}
		Mat newDisplayMat = new Mat(inputFrame.height(), inputFrame.width(),
				CvType.CV_8UC3);
		if (showFullImage) {
			newDisplayMat = currentDisplayMat.clone();
		}

		if (currentDisplayMat == null) {
			return newDisplayMat;
		}

		drawAllFaces(newDisplayMat, faceDetector);
		drawFaces(newDisplayMat, faceRectange);

		if (showROI) {
			drawAreaOfInterest(newDisplayMat,
					mainHumanDetectorGrid.getAreaOfInterest());

		}
		if (showSkeleton) {
			drawHumanSkeleton(newDisplayMat,
					mainHumanDetectorGrid.getHumanFrame());
		}
		if (showGoodFeatures) {
			drawGridNew(newDisplayMat,
					mainHumanDetectorGrid.getGridObjectMatrix());
			/*
			 * drawGoodFeatures(newDisplayMat,
			 * mainHumanDetector.getGoodFeatures(),
			 * mainHumanDetector.getGoodFeaturesEnd(),
			 * mainHumanDetector.getGoodFeaturesHistory(),
			 * mainHumanDetector.getGoodFeaturesEndHistory(),
			 * mainHumanDetector.getGoodFeatureStatus(),
			 * mainHumanDetector.getGoodFeatureStatusHistory());
			 */
		}
		// display the latest processed matrix
		return newDisplayMat;
	}

	public void onCameraViewStopped() {
		// stop processing thread
		endProcessingThread();
		faceDetector.stopFaceDetection();

		if (currentDisplayMat != null) {
			currentDisplayMat.release();
		}
		Log.v(debugTag, "Camera stops");
	}

	// this will initiate a new thread to process the stack of frames
	// this thread will run periodically
	private void startThreadToProcessFrame() {
		processFrameTask = new TimerTask() {
			@Override
			public void run() {
				try {
					Log.v(debugTag,
							"process frame now " + listOfImageMatrix.size());
					// if the stack is empty, there is nothing to process
					if (listOfImageMatrix.isEmpty()) {
						return;
					}
					// get the latest frame
					Mat latestMat = listOfImageMatrix.get(listOfImageMatrix
							.size() - 1);

					// send the latest frame to process, together with other
					// parameters
					isMovementSignificant = mainHumanDetectorGrid.processFrame(
							latestMat, faceRectange, hipPosition, screenSize);

					// update the currentDisplayMat so that it will be drawed
					// later
					currentDisplayMat = latestMat;

					// this is only used with SVM part
					/*
					 * if (mainHumanDetectorGrid.getNormalizedHumanFrame() !=
					 * null) { if (mainHumanTrajectory.getTotalFrames() >= 10) {
					 * mainHumanTrajectory.removeFirst(); }
					 * mainHumanTrajectory.addLast(mainHumanDetectorGrid
					 * .getNormalizedHumanFrame()); }
					 */

					// clear all frame in the stack
					listOfImageMatrix.clear();
				} catch (Exception e) {
					listOfImageMatrix.clear();
					e.printStackTrace();
					Log.v(debugTag,
							"Exception process Frame: " + e.getMessage());
				}
			}
		};
		// A timer will run this task at fixed rate
		timerProcessFrame.scheduleAtFixedRate(processFrameTask,
				Constant.DELAY_BEFORE_PROCESSING_FRAME,
				Constant.PERIOD_PROCESSING_FRAME);
	}

	// this is only used with SVM part
	/*
	 * private void startThreadToDetectMotion() { detectMotionTask = new
	 * TimerTask() {
	 * 
	 * @Override public void run() { try { // if (isMovementSignificant) { { if
	 * (mainHumanTrajectory.getTotalFrames() < 10) { return; }
	 * 
	 * Log.v(debugTag, "START FINDING ACTION"); for (int counter = 0; counter <
	 * mainHumanTrajectory .getTotalFrames(); counter++) { mainHumanTrajectory
	 * .getHumanFrame(counter); }
	 * 
	 * 
	 * if (MotionCheckerPool.isMotion( MotionType.RAISE_HAND_RIGHT,
	 * mainHumanTrajectory)) { Log.v("JAE", "Result: Raise right Hand " +
	 * textCounter); latestAction = HumanAction.RaiseRightHand; if (textCounter
	 * <= 0) { textCounter = 50; } // singlePhoneActionListener.onActionDetected
	 * (HumanAction.RaiseRightHand);
	 * 
	 * 
	 * } else if (MotionCheckerPool.isMotion(MotionType.RAISE_HAND_LEFT,
	 * mainHumanTrajectory)) { Log.v("JAE", "Result: Raise left Hand " +
	 * textCounter); latestAction = HumanAction.RaiseLeftHand; if (textCounter
	 * <= 0) { textCounter = 50; }
	 * 
	 * } else if (textCounter <= 0) { // Log.v("JAE", "Result: Not Raise Hand");
	 * latestAction = HumanAction.None; }
	 * 
	 * Log.v(debugTag, "END FINDING ACTION"); } } catch (Exception e) {
	 * e.printStackTrace(); Log.v(debugTag, "Exception: " + e.toString()); } }
	 * }; // A timer will run this task at fixed rate
	 * timerDetectMotion.scheduleAtFixedRate(detectMotionTask,
	 * Constant.DELAY_BEFORE_PROCESSING_FRAME, Constant.PERIOD_CHECKING_MOTION);
	 * 
	 * }
	 */

	private void drawFaces(Mat resultMat, org.opencv.core.Rect facePosition) {
		if (facePosition == null) {
			return;
		}
		Core.rectangle(resultMat, faceRectange.tl(), faceRectange.br(),
				new Scalar(0, 255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);
		Log.v(debugTag,
				"Face at " + faceRectange.tl() + " " + faceRectange.br());
	}

	private void drawAllFaces(Mat resultMat, FaceDetector faceDetector) {
		if (faceDetector == null) {
			return;
		}
		org.opencv.core.Rect[] arrayOfFaces = faceDetector.getArrayOfFaces();
		if (arrayOfFaces == null || arrayOfFaces.length == 0) {
			return;
		}
		Log.v(debugTag, "Number of faces " + arrayOfFaces.length);
		for (int i = 0; i < arrayOfFaces.length; i++) {
			Rect elementRec = arrayOfFaces[i];
			if (elementRec != null) {
				Core.rectangle(resultMat, elementRec.tl(), elementRec.br(),
						new Scalar(255, 255, 0), frameHeight
								/ Constant.BRUSH_WIDTH_RATIO);
				Log.v(debugTag, "Face index " + i);
			}
		}
		Log.v(debugTag, "Camera frame size " + resultMat.size().width + " "
				+ resultMat.size().height);
	}

	private void drawAreaOfInterest(Mat resultMat,
			ArrayList<Rect> areaOfInterest) {
		if (areaOfInterest == null || areaOfInterest.size() == 0) {
			Log.v(debugTag, "AOI is null or size 0");
			return;
		}
		for (Rect elementRec : areaOfInterest) {
			Core.rectangle(resultMat, elementRec.tl(), elementRec.br(),
					new Scalar(255, 0, 0), frameHeight
							/ Constant.BRUSH_WIDTH_RATIO);
		}
	}

	private void drawGridNew(Mat newDisplayMat,
			ArrayList<ArrayList<Grid>> gridObjectMatrix) {
		if (gridObjectMatrix == null || gridObjectMatrix.size() == 0) {
			return;
		}
		for (ArrayList<Grid> eachList : gridObjectMatrix) {
			for (Grid eachGrid : eachList) {
				if (eachGrid.status > 0) {
					Core.rectangle(newDisplayMat, new Point(eachGrid.xCoor
							- Constant.GRID_SIZE / 2.0, eachGrid.yCoor
							- Constant.GRID_SIZE / 2.0), new Point(
							eachGrid.xCoor + Constant.GRID_SIZE / 2.0,
							eachGrid.yCoor + Constant.GRID_SIZE / 2),
							new Scalar(0, 255, 0));
					for (int index = 0; index < eachGrid.listOfNewNeighbors
							.size(); index++) {
						if (eachGrid.listBestNewNeighborStatus.get(index) != 0) {
							Point eachNeighbor = eachGrid.listNeighborPosition
									.get(index);
							Core.line(newDisplayMat, new Point(eachGrid.xCoor,
									eachGrid.yCoor), eachNeighbor, new Scalar(
									255, 0, 0), 1);
						}
					}
				} else {
					//
				}
			}
		}
	}

	private void drawGrid(Mat resultMat, ArrayList<ArrayList<Rect>> gridMatrix,
			ArrayList<ArrayList<Integer>> gridMatrixStatus,
			ArrayList<ArrayList<Point>> gridMatrixVector) {
		if (gridMatrix == null || gridMatrix.size() == 0) {
			Log.v(debugTag, "GridMatrix is null or size 0");
			return;
		}
		for (int gridCounter = 0; gridCounter < gridMatrix.size(); gridCounter++) {
			ArrayList<Rect> elementList = gridMatrix.get(gridCounter);
			if (elementList == null)
				continue;

			Log.v(debugTag, "Drawing grid " + elementList.size());
			for (int rectCounter = 0; rectCounter < elementList.size(); rectCounter++) {
				Rect eachGrid = elementList.get(rectCounter);
				try {
					if (gridMatrixStatus.get(gridCounter).get(rectCounter) >= 0) {
						Core.rectangle(resultMat, eachGrid.tl(), eachGrid.br(),
								new Scalar(255, 0, 0), 1);
						Point center = new Point(eachGrid.x
								+ eachGrid.size().width / 2, eachGrid.y
								+ eachGrid.size().width / 2);
						double offsetX = gridMatrixVector.get(gridCounter).get(
								rectCounter).x;
						double offsetY = gridMatrixVector.get(gridCounter).get(
								rectCounter).y;

						Core.line(resultMat, center, new Point(center.x
								+ offsetX, center.y + offsetY), new Scalar(0,
								255, 0), 2);
					}
				} catch (Exception e) {
					Log.v(debugTag,
							"Exception while drawing grid " + e.toString());
				}
			}
		}
	}

	private void drawGoodFeatures(Mat resultMat, MatOfPoint2f goodFeatures,
			MatOfPoint2f goodFeaturesEnd, MatOfPoint2f goodFeaturesHistory,
			MatOfPoint2f goodFeaturesHistoryEnd, ArrayList<Integer> status,
			ArrayList<Integer> historyStatus) {
		if (resultMat == null || goodFeatures == null
				|| goodFeaturesHistory == null) {
			return;
		}
		for (int index = 0; index < goodFeatures.size().height; index++) {
			try {
				org.opencv.core.Point startingPointSupport = new org.opencv.core.Point(
						goodFeatures.get(index, 0)[0], goodFeatures.get(index,
								0)[1]);

				if (status.get(index) == 1) {
					org.opencv.core.Point endPointSupport = new org.opencv.core.Point(
							goodFeaturesEnd.get(index, 0)[0],
							goodFeaturesEnd.get(index, 0)[1]);
					Core.line(resultMat, startingPointSupport, endPointSupport,
							new Scalar(0, 255, 0), 1);

				} else if (status.get(index) == 0) {
					Core.circle(resultMat, startingPointSupport, 1, new Scalar(
							255, 0, 0), 1);
				} else {
					Core.circle(resultMat, startingPointSupport, 1, new Scalar(
							255, 255, 0), 1);
				}
			} catch (Exception e) {
				continue;
			}
		}
	}

	private void drawHumanSkeleton(Mat resultMat, HumanFrame humanFrame) {
		if (resultMat == null || humanFrame == null) {
			return;
		}
		if (textCounter > 0) {

			if (latestAction == HumanAction.RaiseRightHand) {

				Core.putText(resultMat, "Raise Right Hand", new Point(20, 200),
						1, 1.5, new Scalar(255, 0, 0), 2);

			} else if (latestAction == HumanAction.RaiseLeftHand) {

				Core.putText(resultMat, "Raise Left Hand", new Point(20, 200),
						1, 1.5, new Scalar(255, 0, 0), 2);

			}
			textCounter--;
		}
		Core.circle(resultMat, new Point(humanFrame.getNode(NodeType.LEFT_HAND)
				.getX(), humanFrame.getNode(NodeType.LEFT_HAND).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat,
				new Point(humanFrame.getNode(NodeType.LEFT_ELBOW).getX(),
						humanFrame.getNode(NodeType.LEFT_ELBOW).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);
		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.LEFT_HAND)
				.getX(), humanFrame.getNode(NodeType.LEFT_HAND).getY()),
				new Point(humanFrame.getNode(NodeType.LEFT_ELBOW).getX(),
						humanFrame.getNode(NodeType.LEFT_ELBOW).getY()),
				new Scalar(0, 255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat,
				new Point(humanFrame.getNode(NodeType.RIGHT_HAND).getX(),
						humanFrame.getNode(NodeType.RIGHT_HAND).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat,
				new Point(humanFrame.getNode(NodeType.RIGHT_ELBOW).getX(),
						humanFrame.getNode(NodeType.RIGHT_ELBOW).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat,
				new Point(humanFrame.getNode(NodeType.HEAD).getX(), humanFrame
						.getNode(NodeType.HEAD).getY()), new Point(humanFrame
						.getNode(NodeType.LEFT_ELBOW).getX(), humanFrame
						.getNode(NodeType.LEFT_ELBOW).getY()), new Scalar(0,
						255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.RIGHT_ELBOW)
				.getX(), humanFrame.getNode(NodeType.RIGHT_ELBOW).getY()),
				new Point(humanFrame.getNode(NodeType.HEAD).getX(), humanFrame
						.getNode(NodeType.HEAD).getY()), new Scalar(0, 255, 0),
				frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.RIGHT_ELBOW)
				.getX(), humanFrame.getNode(NodeType.RIGHT_ELBOW).getY()),
				new Point(humanFrame.getNode(NodeType.RIGHT_HAND).getX(),
						humanFrame.getNode(NodeType.RIGHT_HAND).getY()),
				new Scalar(0, 255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.HIP).getX(),
				humanFrame.getNode(NodeType.HIP).getY()),
				new Point(humanFrame.getNode(NodeType.HEAD).getX(), humanFrame
						.getNode(NodeType.HEAD).getY()), new Scalar(0, 255, 0),
				frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat, new Point(humanFrame.getNode(NodeType.HIP)
				.getX(), humanFrame.getNode(NodeType.HIP).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat, new Point(humanFrame.getNode(NodeType.LEFT_FOOT)
				.getX(), humanFrame.getNode(NodeType.LEFT_FOOT).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat, new Point(humanFrame.getNode(NodeType.LEFT_KNEE)
				.getX(), humanFrame.getNode(NodeType.LEFT_KNEE).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat,
				new Point(humanFrame.getNode(NodeType.RIGHT_FOOT).getX(),
						humanFrame.getNode(NodeType.RIGHT_FOOT).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.circle(resultMat,
				new Point(humanFrame.getNode(NodeType.RIGHT_KNEE).getX(),
						humanFrame.getNode(NodeType.RIGHT_KNEE).getY()), 10,
				new Scalar(0, 255, 255), frameHeight
						/ Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.LEFT_FOOT)
				.getX(), humanFrame.getNode(NodeType.LEFT_FOOT).getY()),
				new Point(humanFrame.getNode(NodeType.LEFT_KNEE).getX(),
						humanFrame.getNode(NodeType.LEFT_KNEE).getY()),
				new Scalar(0, 255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.RIGHT_FOOT)
				.getX(), humanFrame.getNode(NodeType.RIGHT_FOOT).getY()),
				new Point(humanFrame.getNode(NodeType.RIGHT_KNEE).getX(),
						humanFrame.getNode(NodeType.RIGHT_KNEE).getY()),
				new Scalar(0, 255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.HIP).getX(),
				humanFrame.getNode(NodeType.HIP).getY()),
				new Point(humanFrame.getNode(NodeType.LEFT_KNEE).getX(),
						humanFrame.getNode(NodeType.LEFT_KNEE).getY()),
				new Scalar(0, 255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);

		Core.line(resultMat, new Point(humanFrame.getNode(NodeType.HIP).getX(),
				humanFrame.getNode(NodeType.HIP).getY()),
				new Point(humanFrame.getNode(NodeType.RIGHT_KNEE).getX(),
						humanFrame.getNode(NodeType.RIGHT_KNEE).getY()),
				new Scalar(0, 255, 0), frameHeight / Constant.BRUSH_WIDTH_RATIO);
	}

	public void endProcessingThread() {
		processFrameTask.cancel();
	}

	public List<android.hardware.Camera.Size> getListOfPreviewSize() {
		Camera mCamera = Camera.open(1); // the front camera
		Parameters cameraParameter = mCamera.getParameters();
		List<android.hardware.Camera.Size> previewSizes = cameraParameter
				.getSupportedPreviewSizes();

		mCamera.release();
		return previewSizes;
	}

	public void setPreviewSize(Size size) {
		mCameraView.setMaxFrameSize(size.width + 1, size.height + 1);
		mCameraView.setMinimumHeight(size.height - 1);
		mCameraView.setMinimumWidth(size.width - 1);
		frameHeight = size.height;
		screenSize = size;
	}

	public void setShowROI(boolean checked) {
		showROI = checked;
	}

	public void setShowPoints(boolean checked) {
		showGoodFeatures = checked;
	}

	public void setShowFullImage(boolean checked) {
		showFullImage = checked;
	}

	public void startProcessFrame() {
	}

	public void setShowSkeleton(boolean checked) {
		showSkeleton = checked;
	}

	public void setProcessFullBody(boolean checked) {
		mainHumanDetectorGrid.setProcessFullBody(checked);
	}
}
