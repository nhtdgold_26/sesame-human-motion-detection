package sesame.actiondetection;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import android.content.Context;
import android.util.Log;

public class FaceDetector {
	private CascadeClassifier face_cascade;
	private File mCascadeFile;

	private int mAbsoluteFaceSize = 0;

	private TimerTask taskToDetectFace;
	private Timer timerProcess = new Timer();
	private ArrayBlockingQueue<Mat> listOfMatInput;
	private String debugTag = "FACE_DETECTION";
	private Rect[] resultRectArray;
	public Rect correctFace;
	public float minSizeOfFace = 0.0f;
	public float maxSizeOfFace = 0.0f;
	private ArrayBlockingQueue<Rect> listOfCorrectFaces;

	public FaceDetector() {
		// TODO Auto-generated constructor stub
	}

	public void setUpFaceDetector(Context context,
			float minFaceSize, float maxFaceSize) {

		Log.i(debugTag, "OpenCV loaded successfully");

		// Load native library after(!) OpenCV initialization
		//System.loadLibrary("detection_based_tracker");
		 //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	        
		try {
			// load cascade file from application resources
			
			InputStream is = context.getResources().openRawResource(
					context.getResources().getIdentifier("lbpcascade_frontalface", "raw", context.getPackageName()));
			File cascadeDir = context.getDir("cascade", Context.MODE_PRIVATE);
			mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
			FileOutputStream os = new FileOutputStream(mCascadeFile);
			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = is.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			is.close();
			os.close();

			face_cascade = new CascadeClassifier(mCascadeFile.getAbsolutePath());
			if (face_cascade.empty()) {
				Log.v(debugTag, "--(!)Error loading A\n");
				return;
			} else {
				Log.v(debugTag, "Loaded cascade classifier from "
						+ mCascadeFile.getAbsolutePath());
			}

			cascadeDir.delete();

			setMinFaceSize(minFaceSize);
			minSizeOfFace = minFaceSize;
			maxSizeOfFace = maxFaceSize;
			correctFace = new Rect(100, 20, (int) minSizeOfFace, (int) minFaceSize);
			setUpTimerTask();
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(debugTag, "Failed to load cascade. Exception thrown: " + e);
		}
	}

	private void setUpTimerTask() {
		listOfMatInput = new ArrayBlockingQueue<Mat>(
				Constant.MAX_MATRIX_LIST_SIZE);
		listOfCorrectFaces = new ArrayBlockingQueue<Rect>(
				Constant.LIST_OF_CORRECT_FACES_SIZE);
		resultRectArray = new Rect[Constant.MAX_MATRIX_LIST_SIZE];

		taskToDetectFace = new TimerTask() {

			@Override
			public void run() {
				if (listOfMatInput.size() < 2) {
					return;
				}
				Log.v(debugTag, "START DETECT FACE");
				resultRectArray = detectFace(listOfMatInput, resultRectArray);
				correctFace = getCorrectFace(resultRectArray);
				Log.v(debugTag, "END DETECT FACE");
			}
		};

		timerProcess.scheduleAtFixedRate(taskToDetectFace,
				Constant.DELAY_BEFORE_PROCESSING_FRAME,
				Constant.PERIOD_PROCESSING_FACE_DETECTION);

	}

	protected Rect getCorrectFace(Rect[] inputArrayOfRect) {
		if (inputArrayOfRect == null || inputArrayOfRect.length == 0) {
			return correctFace;
		}
		int newRecX = 0, newRecY = 0, newRecWidth = 0, newRecHeight = 0;
		int count = 0;
		for (Rect elementRect : inputArrayOfRect) {
			if (elementRect.y < maxSizeOfFace) {
				newRecX += elementRect.x;
				newRecY += elementRect.y;
				newRecWidth += elementRect.width;
				newRecHeight += elementRect.height;
				count++;
			}
		}
		Rect newRec;
		try {
			if (count > 0) {
				newRec = new Rect(newRecX / count, newRecY / count, newRecWidth
						/ count, newRecHeight / count);
				listOfCorrectFaces.put(newRec);
			} else {
				return correctFace;
			}
		} catch (InterruptedException e) {
			Log.v(debugTag, e.toString());
		}

		count = 0;
		Rect newCorrectFace = new Rect(0, 0, 0, 0);
		for (Rect elementRect : listOfCorrectFaces) {
			newCorrectFace.x += elementRect.x;
			newCorrectFace.y += elementRect.y;
			newCorrectFace.height += elementRect.height;
			newCorrectFace.width += elementRect.width;
			count++;
		}
		newCorrectFace.x /= count;
		newCorrectFace.y /= count;
		newCorrectFace.height /= count;
		newCorrectFace.width /= count;

		correctFace.x += (int) ((newCorrectFace.x - correctFace.x) / Constant.FACE_CORRECTION_FACTOR);
		correctFace.y += (int) ((newCorrectFace.y - correctFace.y) / Constant.FACE_CORRECTION_FACTOR);
		correctFace.width += (int) ((newCorrectFace.width - correctFace.width) / Constant.FACE_CORRECTION_FACTOR);
		correctFace.height += (int) ((newCorrectFace.height - correctFace.height) / Constant.FACE_CORRECTION_FACTOR);

		if (listOfCorrectFaces.size() == Constant.LIST_OF_CORRECT_FACES_SIZE) {
			listOfCorrectFaces.poll();
		}
		return correctFace;
	}

	public Rect[] detectFace(ArrayBlockingQueue<Mat> inputList,
			Rect[] originalResultRect) {
		int listSize = inputList.size();
		for (int index = 0; index < listSize - 1; index++) {
			inputList.poll();
		}
		Mat inputMatrix = inputList.poll();

		Mat mGray = new Mat(inputMatrix.height(), inputMatrix.width(),
				CvType.CV_8UC1);
		Imgproc.cvtColor(inputMatrix, mGray, Imgproc.COLOR_RGB2GRAY);

		MatOfRect faces = new MatOfRect();

		if (face_cascade != null) {
			face_cascade.detectMultiScale(mGray, faces, 1.1, 2, 2, new Size(
					(int) minSizeOfFace, (int) minSizeOfFace), new Size(
					(int) maxSizeOfFace, (int) maxSizeOfFace));
		}
		Rect[] facesArray = faces.toArray();
		Log.v(debugTag, "end detecting faces");
		return facesArray;
	}

	private void setMinFaceSize(float faceSize) {
		mAbsoluteFaceSize = (int) faceSize;
	}

	public void addMatToProcess(Mat inputMat) {
		try {
			listOfMatInput.put(inputMat);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Log.v(debugTag, "enqueue error: " + e.getMessage());
		}
	}

	public void stopFaceDetection() {
		listOfMatInput.clear();
		taskToDetectFace.cancel();
	}

	public Rect[] getArrayOfFaces() {
		return resultRectArray;
	}

	public Rect getCorrectFace() {
		return correctFace;
	}
}
