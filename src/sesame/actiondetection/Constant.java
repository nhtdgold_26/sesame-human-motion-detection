package sesame.actiondetection;

enum ClusterType{
	HEAD, LEFT_ARM, LEFT_HAND, RIGHT_ARM, RIGHT_HAND, HIP, LEFT_KNEE, LEFT_FOOT, RIGHT_KNEE, RIGHT_FOOT, NONE
}
public class Constant {
	
	//Finding features constants
	public static final int MAX_ELEMENT_COUNT = 10;
	public static final int MAX_CORNER_COUNT = 700;
	public static final double MIN_NUMBER_OF_MOVING_POINT = 5;
	public static final int MAX_SIZE_CALIBRATION = 10;
	public static final int NUMBER_OF_SKIPED_FRAMES = 3;
	public static final double FINDING_FEATURES_QUALITY_LEVEL = 0.001;
	public static final double FINDING_FEATURES_MIN_EIGENVALUE = 0.5;
	
	public static final long DELAY_BEFORE_PROCESSING_FRAME = 0;
	public static final long PERIOD_PROCESSING_FRAME = 10;	
	public static final long PERIOD_CHECKING_MOTION = 50;
	
	public static final double GRID_SIZE_FACTOR = 0.3;
	public static final int GRID_MIN_POINT_COUNT = 1;
	public static final double GRID_SIZE = 10;
	public static final int GRID_NEIGHBOR_SIZE = 1;

	//TODO what to write here?
	public static final double MINIMUM_CHANGE_TO_BE_DETECTED = 25;
	
	//SVM Constants
	public static final int NUMBER_OF_TRAJECTORY_MATRIX = 10;	
	
	//Human body nodes position
	public static final double[] OFFSET_X_FACTOR_LIST = {0.0,
														-0.15, -0.35,
														0.15, 0.35,
														0.0,
														-0.14, -0.25,
														0.14, 0.25};

	public static final double[] OFFSET_Y_FACTOR_LIST = {0.0,
														0.2, 0.15,
														0.2, 0.15,
														0.5,
														0.75, 0.85,
														0.75, 0.85};
	public static final double[] OFFSET_X_FACTOR_LIST_HIP = {0.0,
														0.0, 0.0,
														0.0, 0.0,
														0.0,
														-0.125, -0.25,
														0.125, 0.25};

	public static final double[] OFFSET_Y_FACTOR_LIST_HIP = {0.0,
														0.0, 0.0,
														0.0, 0.0,
														0.0,
														0.25, 0.35,
														0.25, 0.35};
	public static final double[] WIDTH_FACTOR_LIST = {	0.0,
														0.1, 0.2,
														0.1, 0.2,
														0.08,
														0.13, 0.22,
														0.13, 0.22};
	
	public static final double[] HEIGHT_FACTOR_LIST = {	0.0,
														0.12, 0.4,
														0.12, 0.4,
														0.08,
														0.1, 0.18,
														0.1, 0.18};
	
	public static final double[] X_COOR_DEFAULT_LIST = {0.0,
														-0.2, -0.5,
														0.2, 0.5,
														0.0,
														-0.14, -0.2,
														0.14, 0.2};

	public static final double[] Y_COOR_DEFAULT_LIST = {0.0,
														0.2, 0.4,
														0.2, 0.4,
														0.5,
														0.75, 1,
														0.75, 1};
																										
	public static final double HEAD_X = 0.0;
	public static final double HEAD_Y = 0.0;
	
	public static final double LEFT_ARM_X = -0.15;
	public static final double LEFT_ARM_Y = 0.2;
	
	public static final double RIGHT_ARM_X = 0.15;
	public static final double RIGHT_ARM_Y = 0.2;
	
	public static final double LEFT_HAND_X = -0.5;
	public static final double LEFT_HAND_Y = 0.2;
	
	public static final double RIGHT_HAND_X = 0.5;
	public static final double RIGHT_HAND_Y = 0.2;
	
	public static final double HIP_X = 0.0;
	public static final double HIP_Y = 0.5;
	
	public static final double LEFT_KNEE_X = -0.125;
	public static final double LEFT_KNEE_Y = 0.75;
	
	public static final double RIGHT_KNEE_X = 0.125;
	public static final double RIGHT_KNEE_Y = 0.75;
	
	public static final double LEFT_FOOT_X = -0.2;
	public static final double LEFT_FOOT_Y = 1.0;
	
	public static final double RIGHT_FOOT_X = 0.2;
	public static final double RIGHT_FOOT_Y = 1.0;	
	
	//Face detection constants
	public static final int MAX_MATRIX_LIST_SIZE = 100;	
	public static final long PERIOD_PROCESSING_FACE_DETECTION = 50;
	public static final double FACE_CORRECTION_FACTOR = 1;
	public static final int LIST_OF_CORRECT_FACES_SIZE = 5;
	public static final int HEIGHT_TO_FACE_RATIO = 7;
	public static final int MIN_FACE_RATIO = 10;
	public static final int MAX_FACE_RATIO = 4;
	
	//Clustering constants
	public static final int NUMBER_OF_CLUSTERS = 10;
	public static final int CLUSTERING_TRIALS = 20;

	//Clustering object
	public static final int CLUSTER_DISTANCT_THRESHOLD = 5;
	public static final double MIN_MOVING_FACTOR = 0.2;
	public static final double MAX_MOVING_FACTOR = 1.8;
	public static final double NODE_CORRECTION_FACTOR = 1.2;
	public static final int BRUSH_WIDTH_RATIO = 100;
	public static final double MOVING_SIGNIFICANT_RATIO = 0.5;
	public static final double MOVING_TOO_MUCH = 2;
	
}
