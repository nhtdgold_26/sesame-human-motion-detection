package sesame.actiondetection;

import java.util.Vector;

import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import sg.edu.nus.sesamemultiphone.motioncapture.Node;

public class Cluster {
	Point centreOfCluster;
	Vector<Point> featureList;
	ClusterType clusterType = ClusterType.NONE;
	MatOfPoint2f matrixOfFeatures;
	Rect areaOfInterest;
	Node svmNode;

	Point centreOfClusterSupport;
	Vector<Point> featureListSupport;
	
	
	int[][] matrixOfPixel;
	public Cluster(double x, double y){
		centreOfCluster = new Point(x, y);
		featureList = new Vector<Point>();
		centreOfClusterSupport = new Point(x, y);
		featureListSupport = new Vector<Point>();
		matrixOfFeatures = new MatOfPoint2f();
		//matrixOfPixel = new int[500][500];
	}
	
	public void setCluster(double x, double y){
		centreOfCluster.x = x;
		centreOfCluster.y = y;
	}
	
	public void addFeature(int x, int y){
		if(x < 0 || y < 0){
			return;
		}
		if(matrixOfPixel == null){
			return;
		}
		if(matrixOfPixel[x][y] != 1){
			featureList.add(new Point(x* 1.0 , y*1.0));
			for(int col = -2; col <= 2; col++){
				for(int row = -2; row <= 2; row++){
					if(row < 0 || col < 0){
						break;
					}
					matrixOfPixel[col][row] = 1;
				}
			}			
		}
		//centreOfCluster = findCentreOfCluster();
	}
	
	public void setType(ClusterType inputType){
		clusterType = inputType;
	}
	
	/*public void updateCentreOfCluster(double radius){
		if(featureList == null && featureList.size() == 0){
			return;
		}
		//validateFeatures(radius);
		double sumX = 0, sumY = 0;
		for(Point elementPoint : featureList){
			sumX += elementPoint.x;
			sumY += elementPoint.y;			
		}
		//double size = matrixOfFeatures.size().height;
		double size = featureList.size();
		
		for(int index = 0; index < size; index++){
			sumX += matrixOfFeatures.get(index, 0)[0];
			sumY += matrixOfFeatures.get(index, 0)[1];
		}
		double deltaX = - centreOfCluster.x + sumX/size;
		double deltaY = - centreOfCluster.y + sumY/size;
		centreOfCluster.x += deltaX*1.1;
		centreOfCluster.y += deltaY*1.1;
		//centreOfCluster = new Point(sumX/featureList.size(), sumY/featureList.size());
	}*/
	
	public boolean updateCentreOfClusterSupport(double radius){
		if(featureListSupport == null || featureListSupport.size() < 10){
			return false;
		}
		//validateFeatures(radius);
		double sumX = 0, sumY = 0;
		for(Point elementPoint : featureListSupport){
			sumX += elementPoint.x;
			sumY += elementPoint.y;			
		}
		//double size = matrixOfFeatures.size().height;
		double size = featureListSupport.size();
		
		/*for(int index = 0; index < size; index++){
			sumX += matrixOfFeatures.get(index, 0)[0];
			sumY += matrixOfFeatures.get(index, 0)[1];
		}*/
		if(centreOfClusterSupport == null){
			centreOfClusterSupport = new Point();
		}
		double deltaX = - centreOfClusterSupport.x + sumX/size;
		double deltaY = - centreOfClusterSupport.y + sumY/size;
		centreOfClusterSupport.x += deltaX;
		centreOfClusterSupport.y += deltaY;
		if(Math.abs(deltaX) + Math.abs(deltaY) > Constant.MINIMUM_CHANGE_TO_BE_DETECTED)
			return true;
		//centreOfCluster = new Point(sumX/featureList.size(), sumY/featureList.size());
		return false;
	}
	
	public void validateFeatures(double radius){
		for(Point elementPoint : featureList){
			if(distance(elementPoint, centreOfCluster) > radius){
				featureList.remove(elementPoint);
			}
		}
		
	}
	
	private double distance(Point p1, Point p2){
		return Math.sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
	}

	public void convertFeaturesToMatOfPoint2f() {
			matrixOfFeatures.fromList(featureList);
	}

	public void clearFeatures() {
		featureList = new Vector<Point>();
		//matrixOfPixel = new int[500][500];	
		featureListSupport = new Vector<Point>();
	}

	public void updateAreaOfInterest(org.opencv.core.Point newCenter, int humanHeightInPixel) {
		if(newCenter != null){
			centreOfCluster = newCenter;
		}else{
			centreOfClusterSupport = centreOfCluster;
		}
		switch(clusterType){
			case LEFT_HAND:
				areaOfInterest = new Rect((int)(centreOfCluster.x), 
					(int)(centreOfCluster.y - humanHeightInPixel*4/10), 
					humanHeightInPixel*4/10, 
					humanHeightInPixel*7/10);
				break;	
			case RIGHT_HAND:
				areaOfInterest = new Rect((int)(centreOfCluster.x - humanHeightInPixel*4/10), 
					(int)(centreOfCluster.y - humanHeightInPixel*4/10), 
					humanHeightInPixel*4/10, 
					humanHeightInPixel*7/10);
				break;	
			case LEFT_ARM:
			case RIGHT_ARM: 
				areaOfInterest = new Rect((int)(centreOfCluster.x - humanHeightInPixel/10), 
					(int)(centreOfCluster.y - humanHeightInPixel*2/10), 
					humanHeightInPixel*2/10, 
					humanHeightInPixel*3/10);
				break;
			case HEAD:
			case HIP:				
				areaOfInterest = new Rect((int)(centreOfCluster.x - humanHeightInPixel/10), 
					(int)(centreOfCluster.y - humanHeightInPixel/10), 
					(int)(humanHeightInPixel*2/10), 
					(int)(humanHeightInPixel*2/10));
				break;
			case LEFT_KNEE:				
				areaOfInterest = new Rect((int)(centreOfCluster.x - humanHeightInPixel*2/10), 
					(int)(centreOfCluster.y - humanHeightInPixel*1.5/10), 
					(int)(humanHeightInPixel*3/10), 
					(int)(humanHeightInPixel*3/10));
				break;
			case RIGHT_KNEE:
				areaOfInterest = new Rect((int)(centreOfCluster.x - humanHeightInPixel*1/10), 
					(int)(centreOfCluster.y - humanHeightInPixel*1.5/10), 
					(int)(humanHeightInPixel*3/10), 
					(int)(humanHeightInPixel*3/10));
				break;
			case LEFT_FOOT:				
				areaOfInterest = new Rect((int)(centreOfCluster.x - humanHeightInPixel*5/10), 
					(int)(centreOfCluster.y - humanHeightInPixel*4/10), 
					(int)(humanHeightInPixel*5/10), 
					(int)(humanHeightInPixel*5/10));
				break;
			case RIGHT_FOOT:				
				areaOfInterest = new Rect((int)(centreOfCluster.x - humanHeightInPixel*0/10), 
					(int)(centreOfCluster.y - humanHeightInPixel*4/10), 
					(int)(humanHeightInPixel*5/10), 
					(int)(humanHeightInPixel*5/10));
				break;

			default:
				areaOfInterest = new Rect(0, 0, 0, 0);
				break;
		}
	}
	
}
