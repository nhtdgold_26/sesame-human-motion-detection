package sesame.actiondetection;


import java.util.ArrayList;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;

import sg.edu.nus.sesamemultiphone.motioncapture.HumanFrame;
import android.util.Log;


public class HumanNodeDetector {
	private String debugTag = "HumanNodeDetector";
	private Mat previousGrayFrame = null;    
	private HumanFrame latestHumanFrame;
	private HumanFrame latestHumanFrameNormalized;
	private HumanFrameFactory humanFrameFactory;
	private ArrayList<Rect> areasOfInterest;
	private MatOfPoint2f goodFeaturesMatrix;
	private MatOfPoint2f goodFeaturesCurrentFrame;
	private MatOfPoint2f goodFeaturesHistory;
	private MatOfPoint2f goodFeaturesCurrentFrameFromHistory;
	private ArrayList<Integer> goodFeatureStatus;
	private ArrayList<Integer> goodFeatureStatusHistory;
	private ArrayList<ArrayList<Rect>> gridMatrix;
	private ArrayList<ArrayList<Point>> gridMatrixMovingVector;
	private ArrayList<ArrayList<Integer>> gridMatrixStatus;
	private boolean processFullBody = false;
	
	public HumanNodeDetector() {
		previousGrayFrame = null;
		//latestHumanFrame = getDefaulHumanFrame();
		latestHumanFrame = null;
		areasOfInterest = null;
		goodFeatureStatus = new ArrayList<Integer>();
		goodFeatureStatusHistory = new ArrayList<Integer>();
	}

	public static HumanFrame getDefaulHumanFrame() {
		return HumanFrameFactory.getDefaultHumanFrame();
	}

	public boolean processFrame(Mat inputFrame, Rect facePosition, Point hipPosition) {
		Log.v(debugTag, "***START PROCESSING FRAME***");
		if(inputFrame == null || facePosition == null){
			Log.v(debugTag, "Exception null frame or null face");
		}
		boolean isMovementSignificant = false;
		Mat srcGrayMat = new Mat(inputFrame.height(), inputFrame.width(),
				CvType.CV_8UC1);
		Imgproc.cvtColor(inputFrame, srcGrayMat, Imgproc.COLOR_RGB2GRAY);

		/*
		 * if this is the first frame, copy to mPreviousGray and return
		 */
		if (previousGrayFrame == null) {
			previousGrayFrame = srcGrayMat;
			Log.v(debugTag, "***END PROCESSING 1st FRAME***");
			return isMovementSignificant;
		}
		if(humanFrameFactory == null)
			humanFrameFactory = new HumanFrameFactory(facePosition, hipPosition);
		else
			humanFrameFactory.resetHumanFrameFactory(facePosition, hipPosition);
		areasOfInterest = humanFrameFactory.getAreasOfInterest();
		gridMatrix = humanFrameFactory.getGrid();
		
		/*
		 * find good features from Previous Frame before tracking
		 */
		Log.v(debugTag, "START FINDING GOOD FEATURES");
		MatOfPoint2f matrixOfGoodFeaturesPreviousFrame = getMatrixOfGoodFeatures(
				previousGrayFrame, 
				Constant.MAX_CORNER_COUNT, 
				Constant.FINDING_FEATURES_QUALITY_LEVEL, 
				Constant.FINDING_FEATURES_MIN_EIGENVALUE);
		//goodFeaturesMatrix = matrixOfGoodFeaturesPreviousFrame;
		Log.v(debugTag, "Number of good features " + matrixOfGoodFeaturesPreviousFrame.size().height);
		if (matrixOfGoodFeaturesPreviousFrame.size().height <= Constant.NUMBER_OF_CLUSTERS) {
			latestHumanFrame = HumanFrameFactory.getDefaultHumanFrame();
			previousGrayFrame = srcGrayMat;
			return isMovementSignificant;
		}
		Log.v(debugTag, "END FINDING GOOD FEATURES " + matrixOfGoodFeaturesPreviousFrame.size().height);
		
		//filter good features before tracking
		Log.v(debugTag, "START FILTERING GOOD FEATURES " + matrixOfGoodFeaturesPreviousFrame.size().height);
		matrixOfGoodFeaturesPreviousFrame = humanFrameFactory.filterGoodFeatures(matrixOfGoodFeaturesPreviousFrame);
		Log.v(debugTag, "END FILTERING GOOD FEATURES " + matrixOfGoodFeaturesPreviousFrame.size().height);
		
		/*
		 * perform tracking those good features found above
		 */
		Log.v(debugTag, "START TRACKING FEATURES " +  matrixOfGoodFeaturesPreviousFrame.size().height);
		MatOfByte trackingStatus = new MatOfByte();
		MatOfFloat trackingError = new MatOfFloat();
		goodFeaturesCurrentFrameFromHistory = new MatOfPoint2f();
		
		if(goodFeaturesHistory == null){
			goodFeaturesHistory = matrixOfGoodFeaturesPreviousFrame;
		}

		MatOfPoint2f goodFeaturesAfterTracking = new MatOfPoint2f();
		Video.calcOpticalFlowPyrLK(previousGrayFrame, srcGrayMat, // 2 consecutive images
				matrixOfGoodFeaturesPreviousFrame, 			// input point position in resultFeatures image
				goodFeaturesAfterTracking, 		// output point position
		        trackingStatus,    		// tracking success
		        trackingError);      	// tracking trackingError

		Log.v(debugTag, "END TRACKING FEATURES " + goodFeaturesAfterTracking.size().height);
		
		latestHumanFrame = humanFrameFactory.createHumanFrame(latestHumanFrame, 
				matrixOfGoodFeaturesPreviousFrame, goodFeaturesHistory, goodFeaturesAfterTracking, goodFeaturesCurrentFrameFromHistory, processFullBody);
		//goodFeaturesCurrentFrame = humanFrameFactory.getFeaturesAfterTracking();
		goodFeaturesHistory = goodFeaturesCurrentFrame;
		goodFeaturesMatrix = matrixOfGoodFeaturesPreviousFrame;
		goodFeaturesCurrentFrame = goodFeaturesAfterTracking;
		goodFeatureStatus = humanFrameFactory.getFeatureStatus();
		goodFeatureStatusHistory = humanFrameFactory.getFeatureStatusHistory();
		isMovementSignificant = humanFrameFactory.isMovementSignificant();
		gridMatrixStatus = humanFrameFactory.getGridStatus();
		gridMatrixMovingVector = humanFrameFactory.getGridMovingVector();
		latestHumanFrameNormalized = NormalizationHumanFrame.performNormalization(latestHumanFrame, facePosition);
		//save the current gray frame to process later
		previousGrayFrame = srcGrayMat;
		Log.v(debugTag, "***END PROCESSING FRAME***");
		return isMovementSignificant;
	}

	// this function will find a good features to perform tracking and
	// clustering
	// good features comes from background removal and OpenCV API to find good
	// features
	private MatOfPoint2f getMatrixOfGoodFeatures(Mat inputGrayMat, 
			int maxDetectionCount, double qualityLevel,
			double minDistance) {
	
		MatOfPoint2f resultMatrix = new MatOfPoint2f();
		MatOfPoint supportResultMatrix = new MatOfPoint();
		Imgproc.goodFeaturesToTrack(inputGrayMat, supportResultMatrix,
				maxDetectionCount, qualityLevel, minDistance);

		supportResultMatrix.convertTo(resultMatrix, CvType.CV_32FC2);
		return resultMatrix;
	}

	public HumanFrame getHumanFrame() {
		return latestHumanFrame;
	}
	
	public HumanFrame getNormalizedHumanFrame() {
		return latestHumanFrameNormalized;
	}
	
	public ArrayList<Rect> getAreaOfInterest(){
		return areasOfInterest;
	}

	public MatOfPoint2f getGoodFeatures() {
		return goodFeaturesMatrix;
	}

	public MatOfPoint2f getGoodFeaturesEnd() {
		return goodFeaturesCurrentFrame;
	}

	public ArrayList<Integer> getGoodFeatureStatus() {
		return goodFeatureStatus;
	}
	
	public ArrayList<Integer> getGoodFeatureStatusHistory() {
		return goodFeatureStatusHistory;
	}

	public void setProcessFullBody(boolean checked) {
		processFullBody = checked;
		
	}

	public MatOfPoint2f getGoodFeaturesHistory() {
		return goodFeaturesHistory;
	}

	public MatOfPoint2f getGoodFeaturesEndHistory() {
		return goodFeaturesCurrentFrameFromHistory;
	}

	public ArrayList<ArrayList<Rect>> getGrid() {
		return gridMatrix;
	}

	public ArrayList<ArrayList<Integer>> getGridStatus() {
		return gridMatrixStatus;
	}

	public ArrayList<ArrayList<Point>> getGridMovingVector() {
		return gridMatrixMovingVector;
	}
	
}
