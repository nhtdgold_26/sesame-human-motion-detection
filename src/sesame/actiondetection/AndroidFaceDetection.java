package sesame.actiondetection;

import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.util.Log;

public class AndroidFaceDetection implements Camera.FaceDetectionListener {
	private String debugTag = "Android Face Detection";
	private Face[] faceResult;

	public Face[] getAllFaceDetected() {
		return faceResult;
	}

	public void onFaceDetection(Face[] faces, Camera camera) {
		if (faces.length > 0) {
			faceResult = faces;
			Log.v(debugTag, "Android Face Detection " + faces[0]);
		}
	}
}