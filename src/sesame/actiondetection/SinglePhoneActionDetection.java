package sesame.actiondetection;

import java.util.List;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;

import sesame.helper.Callbackable;
import android.content.Context;
import android.hardware.Camera.Size;

/*This class is the only link to Main Activity and 
 * it will be the main API 
 */
public class SinglePhoneActionDetection extends Callbackable{
	SinglePhoneHumanMotionListener singlePhoneActionListener;
	FaceDetector mainFaceDetector;
	CameraViewManager mainCameraViewManager;
	CameraBridgeViewBase mainNativeView;
	
	public SinglePhoneActionDetection(
			Context context, SinglePhoneHumanMotionListener singlePhoneActionListener) {
		this.singlePhoneActionListener = singlePhoneActionListener;
		mainFaceDetector = new FaceDetector();
		//this is only used with SVM part
		//motionCheckerInit(context);
	}
	
	public SinglePhoneActionDetection(
			Context context, SinglePhoneHumanMotionListener singlePhoneActionListener,
			CameraBridgeViewBase nativeView)
	{
		this.singlePhoneActionListener = singlePhoneActionListener;
		mainFaceDetector = new FaceDetector();
		mainNativeView = nativeView;
		mainCameraViewManager = new CameraViewManager(context, nativeView, singlePhoneActionListener);
		//this is only used with SVM part
		//motionCheckerInit(context);
	}
	
	//this is only used with SVM part
	//private void motionCheckerInit(Context context) {
	//	MotionCheckerPool.setup(context);
	//}
	
	@Override
	public void run() {
		System.out.println("SinglePhoneActionDetection Running");
	}

	public void stopCapturing() {
		mainCameraViewManager.stopCapturing();
	}
	
	//the frame will draw Region-Of-Interest if this is called
	public void setShowROI(boolean b) {
		mainCameraViewManager.setShowROI(b);
	}
	
	//the frame will draw full captured view if this is called
	public void setShowFullImage(boolean b) {
		mainCameraViewManager.setShowFullImage(b);		
	}

	public void startProcessFrame() {
	}
	
	public void setPreviewSize(Size size) {
		mainCameraViewManager.setPreviewSize(size);
	}

	public void startCapturing() {
		mainCameraViewManager.startCapturing();
	}

	public CvCameraViewListener getCameraManager() {
		return mainCameraViewManager;
	}

	public List<Size> getListOfPreviewSize() {
		return mainCameraViewManager.getListOfPreviewSize();
	}
	
	//the frame will draw human skeleton if this is called
	public void setShowResultSkeleton(boolean checked) {
		mainCameraViewManager.setShowSkeleton(checked);
	}
	
	//the frame will draw good features if this is called
	public void setShowPoints(boolean b) {
		mainCameraViewManager.setShowPoints(b);
	}
	
	//the HumanNodeDetector will process full body if this is called
	//else it will only process upper body 
	public void setProcessFullBody(boolean b) {
		mainCameraViewManager.setProcessFullBody(b);
	}
}
