package sesame.actiondetection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import sg.edu.nus.sesamemultiphone.motioncapture.HumanFrame;
import sg.edu.nus.sesamemultiphone.motioncapture.Node;
import sg.edu.nus.sesamemultiphone.motioncapture.NodeType;
import sg.edu.nus.sesamemultiphone.motioncapture.exceptions.MissingNodeException;
import android.hardware.Camera.Size;
import android.util.Log;

public class HumanFrameFactoryGrid {
	private String debugTag = "HUMAN_FRAME_FACTORY_GRID";
	private Rect facePosition;
	private HumanFrame humanFrameResult;
	private Node[] arrayOfNodes;
	private static Node[] defaulArrayNode = new Node[Constant.NUMBER_OF_CLUSTERS];
	private ArrayList<Rect> areaOfInterest;
	private ArrayList<Point> nodeDisplacementVector;
	private ArrayList<ArrayList<Double>> nodeDisplacementXList;
	private ArrayList<ArrayList<Double>> nodeDisplacementYList;
	private int[] nodeVectorCount;
	private ArrayList<ArrayList<Point>> featureSupportList;
	private NodeType[] nodeTypeList = { NodeType.HEAD, NodeType.LEFT_ELBOW,
			NodeType.LEFT_HAND, NodeType.RIGHT_ELBOW, NodeType.RIGHT_HAND,
			NodeType.HIP, NodeType.LEFT_KNEE, NodeType.LEFT_FOOT,
			NodeType.RIGHT_KNEE, NodeType.RIGHT_FOOT };
	private boolean isSignificantMoved = false;
	private MatOfPoint2f goodFeatureAfterTracking;
	private ArrayList<Integer> goodFeatureStatus;
	private ArrayList<Integer> goodFeatureStatusHistory;
	private ArrayList<ArrayList<Point>> gridMatrixVector;

	private ArrayList<ArrayList<Grid>> matrixOfGridObjects;
	private ArrayList<ArrayList<ArrayList<Integer>>> mainGridMatrixPrevious;
	private ArrayList<ArrayList<ArrayList<Integer>>> mainGridMatrix;
	private ArrayList<ArrayList<Integer>> mainGridMatrixStatus;
	private ArrayList<Point> listOfGoodGrid;

	public HumanFrameFactoryGrid(Rect faceRect, Size screenSize) {
		mainGridMatrix = new ArrayList<ArrayList<ArrayList<Integer>>>();
		mainGridMatrixPrevious = new ArrayList<ArrayList<ArrayList<Integer>>>();
		mainGridMatrixStatus = new ArrayList<ArrayList<Integer>>();
		gridMatrixVector = new ArrayList<ArrayList<Point>>();
		matrixOfGridObjects = new ArrayList<ArrayList<Grid>>();
		listOfGoodGrid = new ArrayList<Point>();
		int numberOfCols = (int) (screenSize.width / Constant.GRID_SIZE);
		int numberOfRows = (int) (screenSize.height / Constant.GRID_SIZE);

		for (int colIndex = 0; colIndex < numberOfCols; colIndex++) {
			ArrayList<ArrayList<Integer>> gridCols = new ArrayList<ArrayList<Integer>>();
			ArrayList<Integer> gridColStatus = new ArrayList<Integer>();
			ArrayList<Point> gridColVector = new ArrayList<Point>();
			ArrayList<Grid> gridObjectColVector = new ArrayList<Grid>();
			for (int rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
				gridCols.add(new ArrayList<Integer>());
				gridColStatus.add(new Integer(0));
				gridColVector.add(new Point(0, 0));
			}
			mainGridMatrix.add(gridCols);
			mainGridMatrixPrevious.add(gridCols);
			mainGridMatrixStatus.add(gridColStatus);
			gridMatrixVector.add(gridColVector);
		}
		for (int colIndex = 0; colIndex < numberOfCols; colIndex++) {
			ArrayList<Grid> gridObjectColVector = new ArrayList<Grid>();
			for (int rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
				gridObjectColVector.add(new Grid(colIndex * Constant.GRID_SIZE,
						rowIndex * Constant.GRID_SIZE,
						new ArrayList<Integer>(), mainGridMatrix));
			}
			matrixOfGridObjects.add(gridObjectColVector);
		}
		resetHumanFrameFactory(faceRect);
	}

	public void resetHumanFrameFactory(Rect faceRect) {
		facePosition = faceRect;
		arrayOfNodes = new Node[Constant.NUMBER_OF_CLUSTERS];
		areaOfInterest = new ArrayList<Rect>();
		nodeDisplacementVector = new ArrayList<Point>();
		nodeDisplacementXList = new ArrayList<ArrayList<Double>>();
		nodeDisplacementYList = new ArrayList<ArrayList<Double>>();
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			nodeDisplacementVector.add(new Point(0, 0));
			nodeDisplacementXList.add(new ArrayList<Double>());
			nodeDisplacementYList.add(new ArrayList<Double>());
		}
		goodFeatureStatus = new ArrayList<Integer>();
		goodFeatureStatusHistory = new ArrayList<Integer>();
		nodeVectorCount = new int[Constant.NUMBER_OF_CLUSTERS];
		featureSupportList = new ArrayList<ArrayList<Point>>();
		updateAreaOfInterest(facePosition, areaOfInterest);
		updateDefaultHumanFrame();

	}

	private void updateAreaOfInterest(Rect faceRect, ArrayList<Rect> areaToTrack) {
		int faceSize = faceRect.width;
		int humanHeight = faceSize * Constant.HEIGHT_TO_FACE_RATIO;
		int gridSize = (int) (faceSize * Constant.GRID_SIZE_FACTOR);
		Point centerOfFace = new Point(faceRect.tl().x / 2.0 + faceRect.br().x
				/ 2.0, faceRect.tl().y / 2.0 + faceRect.br().y / 2.0);
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			// if (counter <= 5) {
			Point centreOfRec = new Point(centerOfFace.x + humanHeight
					* (Constant.OFFSET_X_FACTOR_LIST[counter]), centerOfFace.y
					+ humanHeight * (Constant.OFFSET_Y_FACTOR_LIST[counter]));

			Point topLeft = new Point(centreOfRec.x - humanHeight
					* (Constant.WIDTH_FACTOR_LIST[counter]), centreOfRec.y
					- humanHeight * (Constant.HEIGHT_FACTOR_LIST[counter]));

			Point bottomRight = new Point(centreOfRec.x + humanHeight
					* (Constant.WIDTH_FACTOR_LIST[counter]), centreOfRec.y
					+ humanHeight * (Constant.HEIGHT_FACTOR_LIST[counter]));
			areaToTrack.add(new Rect(topLeft, bottomRight));

			featureSupportList.add(new ArrayList<Point>());
		}
	}

	public HumanFrame createHumanFrame(HumanFrame oldHumanFrame,
			boolean processFullBody) {
		arrayOfNodes = createNodeFromFeatures(oldHumanFrame,
				matrixOfGridObjects, processFullBody);

		try {
			return new HumanFrame(arrayOfNodes);
		} catch (MissingNodeException e) {
			Log.v(debugTag, "Exception MissingNodeException: " + e.toString());
			return null;
		}
	}

	public MatOfPoint2f filterGoodFeatures(MatOfPoint2f goodFeaturesCurrentFrame) {
		return filterFeaturesOnTemplates(goodFeaturesCurrentFrame);
	}

	private Node createNewAverageNodeByGrid(NodeType nodeType,
			ArrayList<Rect> listOfRect, ArrayList<Integer> statusList,
			ArrayList<Point> vectorList) {
		if (listOfRect == null || listOfRect.size() == 0) {
			return null;
		}
		int count = 0;
		double xCoor = 0, yCoor = 0;
		for (int counter = 0; counter < listOfRect.size(); counter++) {
			if (statusList.get(counter) <= 0) {
				continue;
			}
			count++;
			Point eachVector = vectorList.get(counter);
			Rect eachRect = listOfRect.get(counter);
			xCoor += eachRect.x + eachRect.width / 2.0 + eachVector.x;
			yCoor += eachRect.y + eachRect.height / 2.0 + eachVector.y;
		}
		if (count == 0) {
			return null;
		}
		return new Node(nodeType, xCoor / count, yCoor / count);
	}

	private MatOfPoint2f filterFeaturesOnTemplates(
			MatOfPoint2f goodFeaturesCurrentFrame) {
		MatOfPoint2f validMatrixOfGoodFeatures = new MatOfPoint2f();
		Vector<Point> validFeatureList = new Vector<Point>();
		for (int index = 0; index < goodFeaturesCurrentFrame.size().height; index++) {
			org.opencv.core.Point startingPointSupport = new org.opencv.core.Point(
					goodFeaturesCurrentFrame.get(index, 0)[0],
					goodFeaturesCurrentFrame.get(index, 0)[1]);

			for (int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
				if (areaOfInterest.get(counter).contains(startingPointSupport)) {
					validFeatureList.add(startingPointSupport);
					break;
				}
			}
		}
		validMatrixOfGoodFeatures.fromList(validFeatureList);
		return validMatrixOfGoodFeatures;
	}

	private double getMedian(ArrayList<Double> arrayListDoubles) {
		Collections.sort(arrayListDoubles);
		int mid = arrayListDoubles.size() / 2;
		double median = (Double) arrayListDoubles.get(mid);
		if (arrayListDoubles.size() % 2 == 0) {
			median = (median + (Double) arrayListDoubles.get(mid - 1)) / 2;
		}
		return median;
	}

	private Node[] createNodeFromFeatures(HumanFrame oldHumanFrame,
			ArrayList<ArrayList<Grid>> matrixOfGrid, boolean processFullBody) {
		if (matrixOfGrid == null || matrixOfGrid.size() == 0
				|| oldHumanFrame == null) {
			return defaulArrayNode;
		}

		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			if (counter == 0) {
				arrayOfNodes[counter] = new Node(NodeType.HEAD, facePosition.x
						+ facePosition.width / 2, facePosition.y
						+ facePosition.height / 2);
				continue;
			}
			if (!processFullBody && counter > 5) {
				arrayOfNodes[counter] = defaulArrayNode[counter];
				continue;
			}
			Node oldNode = oldHumanFrame.getNode(nodeTypeList[counter]);
			Node newNode = createNewAverageNode(nodeTypeList[counter],
					matrixOfGrid, areaOfInterest.get(counter));
			if (newNode == null) {
				double oldX = forceToBeWithinRange(
						oldNode.getX(),
						areaOfInterest.get(counter).x,
						areaOfInterest.get(counter).x
								+ areaOfInterest.get(counter).width);
				double oldY = forceToBeWithinRange(
						oldNode.getY(),
						areaOfInterest.get(counter).y,
						areaOfInterest.get(counter).y
								+ areaOfInterest.get(counter).height);
				arrayOfNodes[counter] = new Node(oldNode.getNodeType(), oldX,
						oldY);
			} else {
				if (getDistance(newNode, oldNode) > Constant.MOVING_SIGNIFICANT_RATIO
						* facePosition.height) {
					isSignificantMoved = true;
				}
				/*
				 * if (getDistance(newNode, oldNode) > Constant.MOVING_TOO_MUCH
				 * facePosition.height) { Node resultNode = oldNode;
				 * arrayOfNodes[counter] = resultNode; continue; }
				 */
				double newX = oldNode.getX()
						+ (newNode.getX() - oldNode.getX())
						/ Constant.NODE_CORRECTION_FACTOR;

				newX = forceToBeWithinRange(
						newX,
						areaOfInterest.get(counter).x,
						areaOfInterest.get(counter).x
								+ areaOfInterest.get(counter).width);
				double newY = oldNode.getY()
						+ (newNode.getY() - oldNode.getY())
						/ Constant.NODE_CORRECTION_FACTOR;
				newY = forceToBeWithinRange(
						newY,
						areaOfInterest.get(counter).y,
						areaOfInterest.get(counter).y
								+ areaOfInterest.get(counter).height);

				Node resultNode = new Node(oldNode.getNodeType(), newX, newY);
				arrayOfNodes[counter] = resultNode;
			}
		}
		return arrayOfNodes;
	}

	private Node createNewAverageNode(NodeType nodeType,
			ArrayList<ArrayList<Grid>> matrixOfGrid, Rect rectArea) {
		if (matrixOfGrid == null || matrixOfGrid.size() == 0)
			return null;
		double sumX = 0;
		double sumY = 0;
		int counter = 0;
		for (int colIndex = 0; colIndex < matrixOfGrid.size(); colIndex++) {
			ArrayList<Grid> currentColList = matrixOfGrid.get(colIndex);
			for (int rowIndex = 0; rowIndex < currentColList.size(); rowIndex++) {
				Grid currentGrid = currentColList.get(rowIndex);
				if (currentGrid.status != 0) {
					counter++;
					sumX += currentGrid.xCoor;
					sumY += currentGrid.yCoor;
				}

			}
		}
		if (counter == 0)
			return null;
		return new Node(nodeType, sumX / counter, sumY / counter);
	}

	private double forceToBeWithinRange(double value, int min, int max) {
		if (value < min) {
			return min;
		}
		if (value > max) {
			return max;
		}
		return value;
	}

	private Node createNewAverageNode(NodeType nodeType,
			ArrayList<Point> listOfPoint) {
		if (listOfPoint == null
				|| listOfPoint.size() <= Constant.MIN_NUMBER_OF_MOVING_POINT) {
			return null;
		}

		double xCoor = 0, yCoor = 0;
		for (Point eachPoint : listOfPoint) {
			xCoor += eachPoint.x;
			yCoor += eachPoint.y;
		}
		return new Node(nodeType, xCoor / listOfPoint.size(), yCoor
				/ listOfPoint.size());
	}

	private double getDistance(Point endingPointSupport,
			Point startingPointSupport) {
		if (endingPointSupport == null || startingPointSupport == null) {
			return 0;
		}
		double xSquare = (endingPointSupport.x - startingPointSupport.x)
				* (endingPointSupport.x - startingPointSupport.x);
		double ySquare = (endingPointSupport.y - startingPointSupport.y)
				* (endingPointSupport.y - startingPointSupport.y);
		return Math.sqrt(xSquare + ySquare);
	}

	private double getDistance(Node newNode, Node oldNode) {
		if (newNode == null || oldNode == null) {
			return 0;
		}
		double xSquare = (newNode.getX() - oldNode.getX())
				* (newNode.getX() - oldNode.getX());
		double ySquare = (newNode.getY() - oldNode.getY())
				* (newNode.getY() - oldNode.getY());
		return Math.sqrt(xSquare + ySquare);
	}

	public HumanFrame getHumanFrame() {
		return humanFrameResult;
	}

	public ArrayList<Rect> getAreasOfInterest() {
		return areaOfInterest;
	}

	public ArrayList<Integer> getFeatureStatus() {
		return goodFeatureStatus;
	}

	public MatOfPoint2f getFeaturesAfterTracking() {
		return goodFeatureAfterTracking;
	}

	public ArrayList<Integer> getFeatureStatusHistory() {
		return goodFeatureStatusHistory;
	}

	public void updateDefaultHumanFrame() {
		int faceSize = facePosition.width;
		int humanHeight = faceSize * Constant.HEIGHT_TO_FACE_RATIO;
		Point centerOfFace = new Point(facePosition.tl().x / 2.0
				+ facePosition.br().x / 2.0, facePosition.tl().y / 2.0
				+ facePosition.br().y / 2.0);
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			defaulArrayNode[counter] = new Node(nodeTypeList[counter],
					centerOfFace.x + humanHeight
							* Constant.X_COOR_DEFAULT_LIST[counter],
					centerOfFace.y + humanHeight
							* Constant.Y_COOR_DEFAULT_LIST[counter]);
		}
	}

	public static HumanFrame getDefaultHumanFrame() {
		try {
			return new HumanFrame(defaulArrayNode);
		} catch (MissingNodeException e) {
			Log.v("HUMAN_FRAME_FACTORY",
					"Exception MissingNodeException: " + e.toString());
			return null;
		}
	}

	public boolean isMovementSignificant() {
		return isSignificantMoved;
	}

	public ArrayList<ArrayList<ArrayList<Integer>>> getGrid() {
		return mainGridMatrix;
	}

	public ArrayList<ArrayList<Integer>> getGridStatus() {
		return mainGridMatrixStatus;
	}

	public ArrayList<ArrayList<Point>> getGridMovingVector() {
		return gridMatrixVector;
	}

	public boolean setFirstFrame(MatOfPoint2f goodFeatures, Mat srcGrayMat) {
		return assignFeaturesToGrid(goodFeatures, mainGridMatrixPrevious,
				matrixOfGridObjects, srcGrayMat);
	}

	private boolean assignFeaturesToGrid(MatOfPoint2f goodFeatures,
			ArrayList<ArrayList<ArrayList<Integer>>> gridMatrix,
			ArrayList<ArrayList<Grid>> gridObjectMatrix, Mat srcGrayMat) {
		if (goodFeatures == null || goodFeatures.size().height == 0
				|| gridMatrix == null)
			return false;
		for (int colIndex = 0; colIndex < gridMatrix.size(); colIndex++)
			for (int rowIndex = 0; rowIndex < gridMatrix.get(0).size(); rowIndex++)
				gridMatrix.get(colIndex)
						.set(rowIndex, new ArrayList<Integer>());
		for (int rowIndex = 0; rowIndex < goodFeatures.size().height; rowIndex++) {
			org.opencv.core.Point goodFeaturePoint = new org.opencv.core.Point(
					goodFeatures.get(rowIndex, 0)[0], goodFeatures.get(
							rowIndex, 0)[1]);
			double[] pixelValue = srcGrayMat.get(
					(int) goodFeatures.get(rowIndex, 0)[1],
					(int) goodFeatures.get(rowIndex, 0)[0]);
			int gridColIndex = (int) (goodFeaturePoint.x / Constant.GRID_SIZE);
			int gridRowIndex = (int) (goodFeaturePoint.y / Constant.GRID_SIZE);
			try {
				gridMatrix.get(gridColIndex).get(gridRowIndex)
						.add((int) pixelValue[0]);
			} catch (Exception e) {
				break;
			}
		}
		for (int colIndex = 0; colIndex < gridMatrix.size(); colIndex++)
			for (int rowIndex = 0; rowIndex < gridMatrix.get(0).size(); rowIndex++)
				gridObjectMatrix
						.get(colIndex)
						.get(rowIndex)
						.setListOfNewFeatures(
								gridMatrix.get(colIndex).get(rowIndex));
		for (int colIndex = 0; colIndex < gridMatrix.size(); colIndex++)
			for (int rowIndex = 0; rowIndex < gridMatrix.get(0).size(); rowIndex++)
				gridObjectMatrix.get(colIndex).get(rowIndex)
						.updateNeighbors(gridMatrix);

		return true;
	}

	public MatOfPoint2f processGoodFeaturesToTrack(
			MatOfPoint2f matrixOfGoodFeaturesPreviousFrame, Mat srcGrayMat) {
		assignFeaturesToGrid(matrixOfGoodFeaturesPreviousFrame, mainGridMatrix,
				matrixOfGridObjects, srcGrayMat);

		for (int colIndex = 0; colIndex < matrixOfGridObjects.size(); colIndex++)
			for (int rowIndex = 0; rowIndex < matrixOfGridObjects.get(0).size(); rowIndex++)
				matrixOfGridObjects.get(colIndex).get(rowIndex).processGrid();

		/*
		 * int numberOfCols = mainGridMatrix.size(); int numberOfRows =
		 * mainGridMatrix.get(0).size(); for (int colIndex = 0; colIndex <
		 * numberOfCols; colIndex++) { for (int rowIndex = 0; rowIndex <
		 * numberOfRows; rowIndex++) { ArrayList<Integer> currentGrid =
		 * mainGridMatrixPrevious.get( colIndex).get(rowIndex);
		 * ArrayList<ArrayList<Integer>> listOfNeighbors = getNeighbor(
		 * colIndex, rowIndex, mainGridMatrix); if (listOfNeighbors == null)
		 * continue; ArrayList<Integer> newGrid = findBestNeighbor(currentGrid,
		 * listOfNeighbors);
		 * 
		 * if(newGrid != null){ mainGridMatrixStatus.get(colIndex).set(rowIndex,
		 * newGrid.size()); } } }
		 */

		return null;
	}

	private ArrayList<Integer> findBestNeighbor(ArrayList<Integer> currentGrid,
			ArrayList<ArrayList<Integer>> listOfNeighbors) {
		if (currentGrid == null || listOfNeighbors == null
				|| listOfNeighbors.size() <= 0)
			return null;
		ArrayList<Integer> newGrid = new ArrayList<Integer>();
		for (ArrayList<Integer> eachNeighbor : listOfNeighbors) {
			if (matchGrid(currentGrid, eachNeighbor)) {
				return eachNeighbor;
			}
		}
		return null;
	}

	private boolean matchGrid(ArrayList<Integer> currentGrid,
			ArrayList<Integer> eachNeighbor) {
		if (currentGrid == null || eachNeighbor == null)
			return false;
		boolean matchFound = true;
		for (Integer eachPixelValue : currentGrid) {
			for (Integer eachPixelOfNeighbor : eachNeighbor)
				if (Math.abs(eachPixelOfNeighbor - eachPixelValue) < 5)
					break;
				else
					matchFound = false;
		}
		return matchFound;
	}

	private ArrayList<ArrayList<Integer>> getNeighbor(int colIndex,
			int rowIndex, ArrayList<ArrayList<ArrayList<Integer>>> gridMatrix) {
		if (colIndex < 0 || rowIndex < 0 || gridMatrix == null
				|| gridMatrix.size() <= 0) {
			return null;
		}
		int startRowIndex = Math.max(0, rowIndex - Constant.GRID_NEIGHBOR_SIZE);
		int endRowIndex = Math.min(gridMatrix.size(), rowIndex
				+ Constant.GRID_NEIGHBOR_SIZE);
		int startColIndex = Math.max(0, colIndex - Constant.GRID_NEIGHBOR_SIZE);
		int endColIndex = Math.min(gridMatrix.get(0).size(), colIndex
				+ Constant.GRID_NEIGHBOR_SIZE);
		ArrayList<ArrayList<Integer>> returnList = new ArrayList<ArrayList<Integer>>();
		for (int col = startColIndex; col <= endColIndex; col++)
			for (int row = startRowIndex; row <= endRowIndex; row++)
				returnList.add(gridMatrix.get(col).get(row));

		return returnList;
	}

	class Grid {
		int xCoor = 0;
		int yCoor = 0;
		ArrayList<Integer> listOfOldFeatures;
		ArrayList<Integer> listOfNewFeatures;
		ArrayList<ArrayList<Integer>> listOfNewNeighbors;
		ArrayList<Integer> bestNewNeighbor;
		ArrayList<Integer> listBestNewNeighborStatus;
		ArrayList<Point> listNeighborPosition;
		Point bestNewNiegborPosition;
		int status = 0;

		public Grid(double x, double y, ArrayList<Integer> features,
				ArrayList<ArrayList<ArrayList<Integer>>> gridMatrix) {
			xCoor = (int) x;
			yCoor = (int) y;
			listOfOldFeatures = features;
			listNeighborPosition = new ArrayList<Point>();
			listBestNewNeighborStatus = new ArrayList<Integer>();
			listOfNewNeighbors = getNeighbor(gridMatrix);
			bestNewNeighbor = new ArrayList<Integer>();
			bestNewNiegborPosition = new Point();
		}

		public void setListOfNewFeatures(ArrayList<Integer> newFeaturesList) {
			listOfNewFeatures = newFeaturesList;
		}

		public void updateNeighbors(
				ArrayList<ArrayList<ArrayList<Integer>>> gridMatrix) {
			int startColIndex = (int) Math.max(0, xCoor / Constant.GRID_SIZE
					- Constant.GRID_NEIGHBOR_SIZE);
			int endColIndex = (int) Math.min(gridMatrix.size()
					- Constant.GRID_NEIGHBOR_SIZE, xCoor / Constant.GRID_SIZE
					+ Constant.GRID_NEIGHBOR_SIZE);
			int startRowIndex = (int) Math.max(0, yCoor / Constant.GRID_SIZE
					- Constant.GRID_NEIGHBOR_SIZE);
			int endRowIndex = (int) Math.min(gridMatrix.get(0).size()
					- Constant.GRID_NEIGHBOR_SIZE, yCoor / Constant.GRID_SIZE
					+ Constant.GRID_NEIGHBOR_SIZE);
			listOfNewNeighbors = new ArrayList<ArrayList<Integer>>();
			for (int col = startColIndex; col <= endColIndex; col++)
				for (int row = startRowIndex; row <= endRowIndex; row++)
					if (col != xCoor / Constant.GRID_SIZE
							|| row != yCoor / Constant.GRID_SIZE) {
						listOfNewNeighbors.add(gridMatrix.get(col).get(row));
					}
		}

		private ArrayList<ArrayList<Integer>> getNeighbor(
				ArrayList<ArrayList<ArrayList<Integer>>> gridMatrix) {
			if (gridMatrix == null || gridMatrix.size() == 0)
				return null;
			int startColIndex = (int) Math.max(0, xCoor / Constant.GRID_SIZE
					- Constant.GRID_NEIGHBOR_SIZE);
			int endColIndex = (int) Math.min(gridMatrix.size()
					- Constant.GRID_NEIGHBOR_SIZE, xCoor / Constant.GRID_SIZE
					+ Constant.GRID_NEIGHBOR_SIZE);
			int startRowIndex = (int) Math.max(0, yCoor / Constant.GRID_SIZE
					- Constant.GRID_NEIGHBOR_SIZE);
			int endRowIndex = (int) Math.min(gridMatrix.get(0).size()
					- Constant.GRID_NEIGHBOR_SIZE, yCoor / Constant.GRID_SIZE
					+ Constant.GRID_NEIGHBOR_SIZE);
			ArrayList<ArrayList<Integer>> returnList = new ArrayList<ArrayList<Integer>>();
			for (int col = startColIndex; col <= endColIndex; col++)
				for (int row = startRowIndex; row <= endRowIndex; row++)
					if (col != xCoor / Constant.GRID_SIZE
							|| row != yCoor / Constant.GRID_SIZE) {
						listNeighborPosition
								.add(new Point(col * Constant.GRID_SIZE, row
										* Constant.GRID_SIZE));
						listBestNewNeighborStatus.add(new Integer(0));
						returnList.add(gridMatrix.get(col).get(row));
					}

			return returnList;
		}

		private ArrayList<Integer> processGrid() {
			if (listOfNewNeighbors == null || listOfNewNeighbors.size() <= 0)
				return null;
			if (listOfOldFeatures.size() < 2) {
				if (listOfNewFeatures != null)
					listOfOldFeatures = listOfNewFeatures;
				status = Math.max(status - 1, 0);
				// status = 0;
				return null;
			}
			Log.v(debugTag,
					"listOfOldFeatures.size()" + listOfOldFeatures.size());
			for (int index = 0; index < listOfNewNeighbors.size(); index++) {
				ArrayList<Integer> eachNeighbor = listOfNewNeighbors.get(index);
				if (matchGrid(eachNeighbor)) {
					bestNewNeighbor = eachNeighbor;
					bestNewNiegborPosition = listNeighborPosition.get(index);
					listBestNewNeighborStatus.set(index, 1);
					status = Math.min(10, status + 1);
					listOfOldFeatures = listOfNewFeatures;
					// return bestNewNeighbor;
				}
			}
			listOfOldFeatures = listOfNewFeatures;
			status = Math.max(status - 1, 0);
			// status = 0;
			return null;
		}

		public ArrayList<Integer> getNeighbor() {
			return bestNewNeighbor;
		}

		private boolean matchGrid(ArrayList<Integer> eachNeighbor) {
			if (eachNeighbor == null || listOfOldFeatures == null
					|| listOfOldFeatures.size() == 0
					|| eachNeighbor.size() == 0)
				return false;
			boolean matchFound = true;
			for (Integer eachPixelValue : listOfOldFeatures) {
				for (Integer eachPixelOfNeighbor : eachNeighbor)
					if (Math.abs(eachPixelOfNeighbor - eachPixelValue) < 5)
						break;
					else
						matchFound = false;
			}
			return matchFound;
		}

	}

	public ArrayList<ArrayList<Grid>> getGridObjectMatrix() {
		return matrixOfGridObjects;
	}
}
