package sesame.actiondetection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import sg.edu.nus.sesamemultiphone.motioncapture.HumanFrame;
import sg.edu.nus.sesamemultiphone.motioncapture.Node;
import sg.edu.nus.sesamemultiphone.motioncapture.NodeType;
import sg.edu.nus.sesamemultiphone.motioncapture.exceptions.MissingNodeException;
import android.util.Log;

public class HumanFrameFactory {
	private String debugTag = "HUMAN_FRAME_FACTORY";
	private Rect facePosition;
	private HumanFrame humanFrameResult;
	private Node[] arrayOfNodes;
	private static Node[] defaulArrayNode = new Node[Constant.NUMBER_OF_CLUSTERS];
	private ArrayList<Rect> areaOfInterest;
	private ArrayList<Point> nodeDisplacementVector;
	private ArrayList<ArrayList<Double>> nodeDisplacementXList;
	private ArrayList<ArrayList<Double>> nodeDisplacementYList;
	private int[] nodeVectorCount;
	private ArrayList<ArrayList<Point>> featureSupportList;
	private NodeType[] nodeTypeList = { NodeType.HEAD, NodeType.LEFT_ELBOW,
			NodeType.LEFT_HAND, NodeType.RIGHT_ELBOW, NodeType.RIGHT_HAND,
			NodeType.HIP, NodeType.LEFT_KNEE, NodeType.LEFT_FOOT,
			NodeType.RIGHT_KNEE, NodeType.RIGHT_FOOT };
	private boolean isSignificantMoved = false;
	private MatOfPoint2f goodFeatureAfterTracking;
	private ArrayList<Integer> goodFeatureStatus;
	private ArrayList<Integer> goodFeatureStatusHistory;
	private ArrayList<ArrayList<Rect>> gridMatrix;
	private ArrayList<ArrayList<Integer>> gridMatrixStatus;
	private ArrayList<ArrayList<Point>> gridMatrixVector;
	private ArrayList<ArrayList<ArrayList<Point>>> gridMatrixMovingVectorList;

	public HumanFrameFactory(Rect faceRect, Point hipPosition) {
		resetHumanFrameFactory(faceRect, hipPosition);
	}

	public void resetHumanFrameFactory(Rect faceRect, Point hipPosition) {
		facePosition = faceRect;
		arrayOfNodes = new Node[Constant.NUMBER_OF_CLUSTERS];
		areaOfInterest = new ArrayList<Rect>();
		nodeDisplacementVector = new ArrayList<Point>();
		nodeDisplacementXList = new ArrayList<ArrayList<Double>>();
		nodeDisplacementYList = new ArrayList<ArrayList<Double>>();
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			nodeDisplacementVector.add(new Point(0, 0));
			nodeDisplacementXList.add(new ArrayList<Double>());
			nodeDisplacementYList.add(new ArrayList<Double>());
		}
		goodFeatureStatus = new ArrayList<Integer>();
		goodFeatureStatusHistory = new ArrayList<Integer>();
		nodeVectorCount = new int[Constant.NUMBER_OF_CLUSTERS];
		featureSupportList = new ArrayList<ArrayList<Point>>();
		gridMatrix = new ArrayList<ArrayList<Rect>>();
		gridMatrixStatus = new ArrayList<ArrayList<Integer>>();
		gridMatrixVector = new ArrayList<ArrayList<Point>>();
		gridMatrixMovingVectorList = new ArrayList<ArrayList<ArrayList<Point>>>();
		updateAreaOfInterest(facePosition, areaOfInterest, hipPosition);
		updateDefaultHumanFrame();
	}

	private void updateAreaOfInterest(Rect faceRect,
			ArrayList<Rect> areaToTrack, Point hipPosition) {
		int faceSize = faceRect.width;
		int humanHeight = faceSize * Constant.HEIGHT_TO_FACE_RATIO;
		int gridSize = (int) (faceSize * Constant.GRID_SIZE_FACTOR);
		Point centerOfFace = new Point(faceRect.tl().x / 2.0 + faceRect.br().x
				/ 2.0, faceRect.tl().y / 2.0 + faceRect.br().y / 2.0);
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			// if (counter <= 5) {
			Point centreOfRec = new Point(centerOfFace.x + humanHeight
					* (Constant.OFFSET_X_FACTOR_LIST[counter]), centerOfFace.y
					+ humanHeight * (Constant.OFFSET_Y_FACTOR_LIST[counter]));

			Point topLeft = new Point(centreOfRec.x - humanHeight
					* (Constant.WIDTH_FACTOR_LIST[counter]), centreOfRec.y
					- humanHeight * (Constant.HEIGHT_FACTOR_LIST[counter]));

			Point bottomRight = new Point(centreOfRec.x + humanHeight
					* (Constant.WIDTH_FACTOR_LIST[counter]), centreOfRec.y
					+ humanHeight * (Constant.HEIGHT_FACTOR_LIST[counter]));
			areaToTrack.add(new Rect(topLeft, bottomRight));

			/*
			 * if(gridMatrixStatus.size() < Constant.NUMBER_OF_CLUSTERS){
			 * gridMatrixStatus.add(new ArrayList<Integer>()); }
			 */

			ArrayList<Rect> newGridSet = new ArrayList<Rect>();
			ArrayList<Integer> newGridSetStatus = new ArrayList<Integer>();
			ArrayList<ArrayList<Point>> newGridSetMovingVector = new ArrayList<ArrayList<Point>>();
			ArrayList<Point> newGridMovingVector = new ArrayList<Point>();
			Log.v(debugTag, "ADDING GRID " + (bottomRight.y - topLeft.y)
					/ gridSize);
			for (int gridRow = 0; gridRow < (bottomRight.y - topLeft.y)
					/ gridSize; gridRow++) {
				for (int gridCol = 0; gridCol < (bottomRight.x - topLeft.x)
						/ gridSize; gridCol++) {
					Point gridTopLeft = new Point(topLeft.x + gridCol
							* gridSize, topLeft.y + gridRow * gridSize);
					Point gridBottomRight = new Point(gridTopLeft.x + gridSize,
							gridTopLeft.y + gridSize);
					newGridSet.add(new Rect(gridTopLeft, gridBottomRight));
					newGridSetStatus.add(0);
					newGridSetMovingVector.add(new ArrayList<Point>());
					newGridMovingVector.add(new Point(0, 0));
				}
			}
			gridMatrix.add(newGridSet);
			gridMatrixStatus.add(newGridSetStatus);
			gridMatrixMovingVectorList.add(newGridSetMovingVector);
			gridMatrixVector.add(newGridMovingVector);
			featureSupportList.add(new ArrayList<Point>());
		}
	}

	public HumanFrame createHumanFrame(HumanFrame oldHumanFrame,
			MatOfPoint2f goodFeaturesPreviousFrame,
			MatOfPoint2f goodFeaturesHistory,
			MatOfPoint2f goodFeaturesCurrentFrame,
			MatOfPoint2f goodFeaturesCurrentFrameFromHistory,
			boolean processFullBody) {
		isSignificantMoved = false;

		/*
		 * filterFeaturesAndUpdateStatus(goodFeaturesPreviousFrame,
		 * goodFeaturesCurrentFrame, processFullBody, goodFeatureStatus,
		 * oldHumanFrame);
		 */
		filterFeaturesAndUpdateStatusByGrid(goodFeaturesPreviousFrame,
				goodFeaturesCurrentFrame, processFullBody, goodFeatureStatus,
				oldHumanFrame);

		/*
		 * filterFeaturesAndUpdateStatus(goodFeaturesHistory,
		 * goodFeaturesCurrentFrameFromHistory, processFullBody,
		 * goodFeatureStatusHistory, oldHumanFrame);
		 */

		/*
		 * arrayOfNodes = createNodeFromFeatures(oldHumanFrame,
		 * featureSupportList, processFullBody);
		 */
		arrayOfNodes = createNodeFromFeaturesByGrid(oldHumanFrame, gridMatrix, gridMatrixStatus, gridMatrixVector, processFullBody);

		try {
			return new HumanFrame(arrayOfNodes);
		} catch (MissingNodeException e) {
			Log.v(debugTag, "Exception MissingNodeException: " + e.toString());
			return null;
		}
	}

	public MatOfPoint2f filterGoodFeatures(MatOfPoint2f goodFeaturesCurrentFrame) {
		return filterFeaturesOnTemplates(goodFeaturesCurrentFrame);
	}

	private void filterFeaturesAndUpdateStatusByGrid(
			MatOfPoint2f goodFeaturesPreviousFrame,
			MatOfPoint2f goodFeaturesCurrentFrame, boolean processFullBody,
			ArrayList<Integer> statusArray, HumanFrame oldHumanFrame) {
		if (goodFeaturesPreviousFrame == null)
			return;
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			nodeDisplacementVector.add(new Point(0, 0));
			nodeDisplacementXList.add(new ArrayList<Double>());
			nodeDisplacementYList.add(new ArrayList<Double>());
		}
		nodeVectorCount = new int[Constant.NUMBER_OF_CLUSTERS];

		for (int index = 0; index < goodFeaturesPreviousFrame.size().height; index++) {
			org.opencv.core.Point startingPointSupport = new org.opencv.core.Point(
					goodFeaturesPreviousFrame.get(index, 0)[0],
					goodFeaturesPreviousFrame.get(index, 0)[1]);

			org.opencv.core.Point endingPointSupport = new org.opencv.core.Point(
					goodFeaturesCurrentFrame.get(index, 0)[0],
					goodFeaturesCurrentFrame.get(index, 0)[1]);

			double movingDistance = getDistance(endingPointSupport,
					startingPointSupport);
			if (movingDistance < 1.0 * facePosition.height
					* Constant.MIN_MOVING_FACTOR
					|| movingDistance > 1.0 * facePosition.height
							* Constant.MAX_MOVING_FACTOR) {
				continue;
			}
			for (int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
				if (!processFullBody && counter > 5) {
					continue;
				}
				if (areaOfInterest.get(counter).contains(endingPointSupport)) {
					org.opencv.core.Point currentNode = new org.opencv.core.Point(
							oldHumanFrame.getNode(nodeTypeList[counter]).getX(),
							oldHumanFrame.getNode(nodeTypeList[counter]).getY());
					if (getDistance(endingPointSupport, currentNode) > 1.0
							* facePosition.height * Constant.MAX_MOVING_FACTOR) {
						continue;
					}
					for (int gridCounter = 0; gridCounter < gridMatrix.get(
							counter).size(); gridCounter++) {
						Rect eachRect = gridMatrix.get(counter)
								.get(gridCounter);
						if (eachRect.contains(startingPointSupport)) {
							gridMatrixStatus.get(counter).set(
									gridCounter,
									gridMatrixStatus.get(counter).get(
											gridCounter) + 1);
							gridMatrixMovingVectorList
									.get(counter)
									.get(gridCounter)
									.add(new Point(endingPointSupport.x
											- startingPointSupport.x,
											endingPointSupport.y
													- startingPointSupport.y));
							break;
						}
					}
					break;
				}
			}
		}
		for (int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			if (!processFullBody && counter > 5) {
				continue;
			}
			for (int gridCounter = 0; gridCounter < gridMatrix.get(counter)
					.size(); gridCounter++) {
				Rect eachRect = gridMatrix.get(counter).get(gridCounter);
				Point gridMovingVector = new Point();
				gridMovingVector = getMovingVectorOfGrid(gridMatrixMovingVectorList
						.get(counter).get(gridCounter));
				// gridMatrixMovingVector.get(counter).set(gridCounter, )
				int numberOfValidFeatures = countNumberOfValidFeaturesInGrid(
						gridMovingVector,
						gridMatrixMovingVectorList.get(counter)
								.get(gridCounter));
				if (numberOfValidFeatures > Constant.GRID_MIN_POINT_COUNT) {
					gridMatrixVector.get(counter).set(gridCounter,
							gridMovingVector);
				} else {
					gridMatrixStatus.get(counter).set(gridCounter, -1);
				}
			}
		}
	}

	private int countNumberOfValidFeaturesInGrid(Point gridMovingVector,
			ArrayList<Point> arrayList) {
		if (arrayList == null || arrayList.size() == 0) {
			return 0;
		}
		int featureCount = 0;
		double angleOfMainVector = Math.atan2(gridMovingVector.y,
				gridMovingVector.x);
		double lengthOfMainVector = Math.sqrt(gridMovingVector.x
				* gridMovingVector.x + gridMovingVector.y * gridMovingVector.y);
		for (Point eachVector : arrayList) {
			/*
			 * if(Math.abs(eachVector.x - gridMovingVector.x) <
			 * 0.5*Math.abs(gridMovingVector.x) && Math.abs(eachVector.y -
			 * gridMovingVector.y) < 0.5*Math.abs(gridMovingVector.y))
			 */
			double vectorAngle = Math.atan2(eachVector.y, eachVector.x);
			double lengthOfEachVector = Math.sqrt(eachVector.x * eachVector.x
					+ eachVector.y * eachVector.y);
			if (Math.abs(lengthOfEachVector - lengthOfMainVector) < 1.0 * lengthOfMainVector
					&& Math.abs(vectorAngle - angleOfMainVector) < Math.PI / 2.0) 
			{
				featureCount++;
			}
		}
		return featureCount;
	}

	private Node[] createNodeFromFeaturesByGrid(HumanFrame oldHumanFrame,
			ArrayList<ArrayList<Rect>> gridMatrix, ArrayList<ArrayList<Integer>> gridStatus,
			ArrayList<ArrayList<Point>> gridVector, boolean processFullBody) {
		if (gridMatrix == null || gridMatrix.size() == 0
				|| oldHumanFrame == null) {
			return defaulArrayNode;
		}

		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			if (counter == 0) {
				arrayOfNodes[counter] = new Node(NodeType.HEAD, facePosition.x
						+ facePosition.width / 2, facePosition.y
						+ facePosition.height / 2);
				continue;
			}
			if (!processFullBody && counter > 5) {
				arrayOfNodes[counter] = defaulArrayNode[counter];
				continue;
			}
			Node oldNode = oldHumanFrame.getNode(nodeTypeList[counter]);
			Node newNode = createNewAverageNodeByGrid(nodeTypeList[counter],
					gridMatrix.get(counter), gridStatus.get(counter), gridVector.get(counter));
			if (newNode == null) {
				double oldX = forceToBeWithinRange(
						oldNode.getX(),
						areaOfInterest.get(counter).x,
						areaOfInterest.get(counter).x
								+ areaOfInterest.get(counter).width);
				double oldY = forceToBeWithinRange(
						oldNode.getY(),
						areaOfInterest.get(counter).y,
						areaOfInterest.get(counter).y
								+ areaOfInterest.get(counter).height);
				arrayOfNodes[counter] = new Node(oldNode.getNodeType(), oldX,
						oldY);
			} else {
				if (getDistance(newNode, oldNode) > Constant.MOVING_SIGNIFICANT_RATIO
						* facePosition.height) {
					isSignificantMoved = true;
				}
				/*
				 * if (getDistance(newNode, oldNode) > Constant.MOVING_TOO_MUCH
				 * facePosition.height) { Node resultNode = oldNode;
				 * arrayOfNodes[counter] = resultNode; continue; }
				 */
				double newX = oldNode.getX()
						+ (newNode.getX() - oldNode.getX())
						/ Constant.NODE_CORRECTION_FACTOR;

				newX = forceToBeWithinRange(
						newX,
						areaOfInterest.get(counter).x,
						areaOfInterest.get(counter).x
								+ areaOfInterest.get(counter).width);
				double newY = oldNode.getY()
						+ (newNode.getY() - oldNode.getY())
						/ Constant.NODE_CORRECTION_FACTOR;
				newY = forceToBeWithinRange(
						newY,
						areaOfInterest.get(counter).y,
						areaOfInterest.get(counter).y
								+ areaOfInterest.get(counter).height);

				Node resultNode = new Node(oldNode.getNodeType(), newX, newY);
				arrayOfNodes[counter] = resultNode;
			}
		}
		return arrayOfNodes;
	}

	private Node createNewAverageNodeByGrid(NodeType nodeType,
			ArrayList<Rect> listOfRect, ArrayList<Integer> statusList, ArrayList<Point> vectorList) {
		if (listOfRect == null
				|| listOfRect.size() == 0) {
			return null;
		}
		int count = 0;
		double xCoor = 0, yCoor = 0;
		for (int counter = 0; counter < listOfRect.size(); counter++){
			if(statusList.get(counter) <= 0){
				continue;
			}
			count++;
			Point eachVector = vectorList.get(counter);
			Rect eachRect = listOfRect.get(counter);
			xCoor += eachRect.x + eachRect.width/2.0 + eachVector.x;
			yCoor += eachRect.y + eachRect.height/2.0 + eachVector.y;
		}
		if(count == 0){
			return null;
		}
		return new Node(nodeType, xCoor / count, yCoor
				/ count);
	}

	private Point getMovingVectorOfGrid(ArrayList<Point> arrayList) {
		if (arrayList == null || arrayList.size() == 0) {
			return new Point(0, 0);
		}
		Point resultVector = new Point(0, 0);
		for (Point eachPoint : arrayList) {
			resultVector.x += eachPoint.x;
			resultVector.y += eachPoint.y;
		}
		resultVector.x /= arrayList.size();
		resultVector.y /= arrayList.size();

		return resultVector;
	}

	private void filterFeaturesAndUpdateStatus(
			MatOfPoint2f goodFeaturesPreviousFrame,
			MatOfPoint2f goodFeaturesCurrentFrame, boolean processFullBody,
			ArrayList<Integer> statusArray, HumanFrame oldHumanFrame) {
		if (goodFeaturesPreviousFrame == null)
			return;
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			nodeDisplacementVector.add(new Point(0, 0));
			nodeDisplacementXList.add(new ArrayList<Double>());
			nodeDisplacementYList.add(new ArrayList<Double>());
		}
		nodeVectorCount = new int[Constant.NUMBER_OF_CLUSTERS];

		for (int index = 0; index < goodFeaturesPreviousFrame.size().height; index++) {
			org.opencv.core.Point startingPointSupport = new org.opencv.core.Point(
					goodFeaturesPreviousFrame.get(index, 0)[0],
					goodFeaturesPreviousFrame.get(index, 0)[1]);

			org.opencv.core.Point endingPointSupport = new org.opencv.core.Point(
					goodFeaturesCurrentFrame.get(index, 0)[0],
					goodFeaturesCurrentFrame.get(index, 0)[1]);

			double movingDistance = getDistance(endingPointSupport,
					startingPointSupport);
			if (movingDistance < 1.0 * facePosition.height
					* Constant.MIN_MOVING_FACTOR
					|| movingDistance > 1.0 * facePosition.height
							* Constant.MAX_MOVING_FACTOR) {
				continue;
			}
			for (int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
				if (!processFullBody && counter > 5) {
					continue;
				}
				if (areaOfInterest.get(counter).contains(endingPointSupport)) {
					org.opencv.core.Point currentNode = new org.opencv.core.Point(
							oldHumanFrame.getNode(nodeTypeList[counter]).getX(),
							oldHumanFrame.getNode(nodeTypeList[counter]).getY());
					if (getDistance(endingPointSupport, currentNode) > 1.0
							* facePosition.height * Constant.MAX_MOVING_FACTOR) {
						continue;
					}
					Point currentNodeVector = nodeDisplacementVector
							.get(counter);
					nodeVectorCount[counter]++;
					nodeDisplacementVector.set(counter, new Point(
							currentNodeVector.x + endingPointSupport.x
									- startingPointSupport.x,
							currentNodeVector.y + endingPointSupport.y
									- startingPointSupport.y));

					nodeDisplacementXList.get(counter).add(
							new Double(endingPointSupport.x
									- startingPointSupport.x));
					nodeDisplacementYList.get(counter).add(
							new Double(endingPointSupport.y
									- startingPointSupport.y));
					break;
				}
			}
		}
		for (int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			if (!processFullBody && counter > 5) {
				continue;
			}
			if (nodeVectorCount[counter] == 0) {
				continue;
			}

			Point currentNodeVector = nodeDisplacementVector.get(counter);
			nodeDisplacementVector.set(counter, new Point(currentNodeVector.x
					/ nodeVectorCount[counter], currentNodeVector.y
					/ nodeVectorCount[counter]));

			nodeDisplacementVector.set(counter, currentNodeVector);
		}

		for (int index = 0; index < goodFeaturesPreviousFrame.size().height; index++) {
			org.opencv.core.Point startingPointSupport = new org.opencv.core.Point(
					goodFeaturesPreviousFrame.get(index, 0)[0],
					goodFeaturesPreviousFrame.get(index, 0)[1]);

			org.opencv.core.Point endingPointSupport = new org.opencv.core.Point(
					goodFeaturesCurrentFrame.get(index, 0)[0],
					goodFeaturesCurrentFrame.get(index, 0)[1]);

			double xDisplacement = endingPointSupport.x
					- startingPointSupport.x;
			double yDisplacement = endingPointSupport.y
					- startingPointSupport.y;
			double movingDistance = getDistance(endingPointSupport,
					startingPointSupport);
			if (movingDistance < 1.0 * facePosition.height
					* Constant.MIN_MOVING_FACTOR
					|| movingDistance > 1.0 * facePosition.height
							* Constant.MAX_MOVING_FACTOR) {
				statusArray.add(0);
				continue;
			}
			boolean found = false;
			for (int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
				if (!processFullBody && counter > 5) {
					continue;
				}
				if (areaOfInterest.get(counter).contains(startingPointSupport)) {
					found = true;
					org.opencv.core.Point currentNode = new org.opencv.core.Point(
							oldHumanFrame.getNode(nodeTypeList[counter]).getX(),
							oldHumanFrame.getNode(nodeTypeList[counter]).getY());
					if (getDistance(endingPointSupport, currentNode) > 1.0
							* facePosition.height * Constant.MAX_MOVING_FACTOR) {
						statusArray.add(-1);
						break;
					}
					for (int gridCounter = 0; gridCounter < gridMatrix.get(
							counter).size(); gridCounter++) {
						Rect eachRect = gridMatrix.get(counter)
								.get(gridCounter);
						if (eachRect.contains(startingPointSupport)) {
							gridMatrixStatus.get(counter).set(
									gridCounter,
									gridMatrixStatus.get(counter).get(
											gridCounter) + 1);
							break;
						}
					}
					if (Math.abs(xDisplacement
							- nodeDisplacementVector.get(counter).x) < Math
								.abs(nodeDisplacementVector.get(counter).x)
							&& Math.abs(yDisplacement
									- nodeDisplacementVector.get(counter).y) < Math
										.abs(nodeDisplacementVector
												.get(counter).y)) {
						featureSupportList.get(counter).add(
								startingPointSupport);
						statusArray.add(1);
						break;
					}

				}
			}
			if (!found) {
				statusArray.add(0);
			}
		}
	}

	private MatOfPoint2f filterFeaturesOnTemplates(
			MatOfPoint2f goodFeaturesCurrentFrame) {
		MatOfPoint2f validMatrixOfGoodFeatures = new MatOfPoint2f();
		Vector<Point> validFeatureList = new Vector<Point>();
		for (int index = 0; index < goodFeaturesCurrentFrame.size().height; index++) {
			org.opencv.core.Point startingPointSupport = new org.opencv.core.Point(
					goodFeaturesCurrentFrame.get(index, 0)[0],
					goodFeaturesCurrentFrame.get(index, 0)[1]);

			for (int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
				if (areaOfInterest.get(counter).contains(startingPointSupport)) {
					validFeatureList.add(startingPointSupport);
					break;
				}
			}
		}
		validMatrixOfGoodFeatures.fromList(validFeatureList);
		return validMatrixOfGoodFeatures;
	}

	private double getMedian(ArrayList<Double> arrayListDoubles) {
		Collections.sort(arrayListDoubles);
		int mid = arrayListDoubles.size() / 2;
		double median = (Double) arrayListDoubles.get(mid);
		if (arrayListDoubles.size() % 2 == 0) {
			median = (median + (Double) arrayListDoubles.get(mid - 1)) / 2;
		}
		return median;
	}

	private Node[] createNodeFromFeatures(HumanFrame oldHumanFrame,
			ArrayList<ArrayList<Point>> featureList, boolean processFullBody) {
		if (featureList == null || featureList.size() == 0
				|| oldHumanFrame == null) {
			return defaulArrayNode;
		}

		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			if (counter == 0) {
				arrayOfNodes[counter] = new Node(NodeType.HEAD, facePosition.x
						+ facePosition.width / 2, facePosition.y
						+ facePosition.height / 2);
				continue;
			}
			if (!processFullBody && counter > 5) {
				arrayOfNodes[counter] = defaulArrayNode[counter];
				continue;
			}
			Node oldNode = oldHumanFrame.getNode(nodeTypeList[counter]);
			Node newNode = createNewAverageNode(nodeTypeList[counter],
					featureList.get(counter));
			if (newNode == null) {
				double oldX = forceToBeWithinRange(
						oldNode.getX(),
						areaOfInterest.get(counter).x,
						areaOfInterest.get(counter).x
								+ areaOfInterest.get(counter).width);
				double oldY = forceToBeWithinRange(
						oldNode.getY(),
						areaOfInterest.get(counter).y,
						areaOfInterest.get(counter).y
								+ areaOfInterest.get(counter).height);
				arrayOfNodes[counter] = new Node(oldNode.getNodeType(), oldX,
						oldY);
			} else {
				if (getDistance(newNode, oldNode) > Constant.MOVING_SIGNIFICANT_RATIO
						* facePosition.height) {
					isSignificantMoved = true;
				}
				/*
				 * if (getDistance(newNode, oldNode) > Constant.MOVING_TOO_MUCH
				 * facePosition.height) { Node resultNode = oldNode;
				 * arrayOfNodes[counter] = resultNode; continue; }
				 */
				double newX = oldNode.getX()
						+ (newNode.getX() - oldNode.getX())
						/ Constant.NODE_CORRECTION_FACTOR;

				newX = forceToBeWithinRange(
						newX,
						areaOfInterest.get(counter).x,
						areaOfInterest.get(counter).x
								+ areaOfInterest.get(counter).width);
				double newY = oldNode.getY()
						+ (newNode.getY() - oldNode.getY())
						/ Constant.NODE_CORRECTION_FACTOR;
				newY = forceToBeWithinRange(
						newY,
						areaOfInterest.get(counter).y,
						areaOfInterest.get(counter).y
								+ areaOfInterest.get(counter).height);

				Node resultNode = new Node(oldNode.getNodeType(), newX, newY);
				arrayOfNodes[counter] = resultNode;
			}
		}
		return arrayOfNodes;
	}

	private double forceToBeWithinRange(double value, int min, int max) {
		if (value < min) {
			return min;
		}
		if (value > max) {
			return max;
		}
		return value;
	}

	private Node createNewAverageNode(NodeType nodeType,
			ArrayList<Point> listOfPoint) {
		if (listOfPoint == null
				|| listOfPoint.size() <= Constant.MIN_NUMBER_OF_MOVING_POINT) {
			return null;
		}

		double xCoor = 0, yCoor = 0;
		for (Point eachPoint : listOfPoint) {
			xCoor += eachPoint.x;
			yCoor += eachPoint.y;
		}
		return new Node(nodeType, xCoor / listOfPoint.size(), yCoor
				/ listOfPoint.size());
	}

	private double getDistance(Point endingPointSupport,
			Point startingPointSupport) {
		if (endingPointSupport == null || startingPointSupport == null) {
			return 0;
		}
		double xSquare = (endingPointSupport.x - startingPointSupport.x)
				* (endingPointSupport.x - startingPointSupport.x);
		double ySquare = (endingPointSupport.y - startingPointSupport.y)
				* (endingPointSupport.y - startingPointSupport.y);
		return Math.sqrt(xSquare + ySquare);
	}

	private double getDistance(Node newNode, Node oldNode) {
		if (newNode == null || oldNode == null) {
			return 0;
		}
		double xSquare = (newNode.getX() - oldNode.getX())
				* (newNode.getX() - oldNode.getX());
		double ySquare = (newNode.getY() - oldNode.getY())
				* (newNode.getY() - oldNode.getY());
		return Math.sqrt(xSquare + ySquare);
	}

	public HumanFrame getHumanFrame() {
		return humanFrameResult;
	}

	public ArrayList<Rect> getAreasOfInterest() {
		return areaOfInterest;
	}

	public ArrayList<Integer> getFeatureStatus() {
		return goodFeatureStatus;
	}

	public MatOfPoint2f getFeaturesAfterTracking() {
		return goodFeatureAfterTracking;
	}

	public ArrayList<Integer> getFeatureStatusHistory() {
		return goodFeatureStatusHistory;
	}

	public void updateDefaultHumanFrame() {
		int faceSize = facePosition.width;
		int humanHeight = faceSize * Constant.HEIGHT_TO_FACE_RATIO;
		Point centerOfFace = new Point(facePosition.tl().x / 2.0
				+ facePosition.br().x / 2.0, facePosition.tl().y / 2.0
				+ facePosition.br().y / 2.0);
		for (int counter = 0; counter < Constant.NUMBER_OF_CLUSTERS; counter++) {
			defaulArrayNode[counter] = new Node(nodeTypeList[counter],
					centerOfFace.x + humanHeight
							* Constant.X_COOR_DEFAULT_LIST[counter],
					centerOfFace.y + humanHeight
							* Constant.Y_COOR_DEFAULT_LIST[counter]);
		}
	}

	public static HumanFrame getDefaultHumanFrame() {
		try {
			return new HumanFrame(defaulArrayNode);
		} catch (MissingNodeException e) {
			Log.v("HUMAN_FRAME_FACTORY",
					"Exception MissingNodeException: " + e.toString());
			return null;
		}
	}

	public boolean isMovementSignificant() {
		return isSignificantMoved;
	}

	public ArrayList<ArrayList<Rect>> getGrid() {
		return gridMatrix;
	}

	public ArrayList<ArrayList<Integer>> getGridStatus() {
		return gridMatrixStatus;
	}

	public ArrayList<ArrayList<Point>> getGridMovingVector() {
		return gridMatrixVector;
	}
}
