package sesame.actiondetection;

import org.opencv.core.Rect;

import sg.edu.nus.sesamemultiphone.motioncapture.HumanFrame;
import sg.edu.nus.sesamemultiphone.motioncapture.Node;
import sg.edu.nus.sesamemultiphone.motioncapture.NodeType;
import sg.edu.nus.sesamemultiphone.motioncapture.exceptions.MissingNodeException;

public class NormalizationHumanFrame {
	private static NodeType[] nodeTypeList = { NodeType.HEAD, NodeType.LEFT_ELBOW,
			NodeType.LEFT_HAND, NodeType.RIGHT_ELBOW, NodeType.RIGHT_HAND,
			NodeType.HIP, NodeType.LEFT_KNEE, NodeType.LEFT_FOOT,
			NodeType.RIGHT_KNEE, NodeType.RIGHT_FOOT };
	
	static HumanFrame performNormalization(HumanFrame inputHumanFrame, Rect faceRect){
		if(inputHumanFrame == null){
			return null;
		}
		int faceSize = faceRect.width;
		int humanHeight = faceSize * Constant.HEIGHT_TO_FACE_RATIO;
		Node [] newArrayNodes = new Node[Constant.NUMBER_OF_CLUSTERS];
		Node faceNode = inputHumanFrame.getNode(NodeType.HEAD);
		newArrayNodes[0] = new Node(NodeType.HEAD, 0, 0);
		
		for(int counter = 1; counter < Constant.NUMBER_OF_CLUSTERS; counter++){
			Node currentNode = inputHumanFrame.getNode(nodeTypeList[counter]);
			double xCoor = 1.0*(currentNode.getX() - faceNode.getX())/humanHeight;
			double yCoor = 1.0*(faceNode.getY() - currentNode.getY())/humanHeight;
			newArrayNodes[counter] = new Node(nodeTypeList[counter], xCoor, yCoor);
		}
		try {
			return new HumanFrame(newArrayNodes);
		} catch (MissingNodeException e) {
			return null;
		}
		
	}
}
