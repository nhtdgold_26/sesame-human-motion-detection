package sesame.actiondetection;

import java.util.ArrayList;

import sg.edu.nus.sesamemultiphone.motioncapture.Node;
import sg.edu.nus.sesamemultiphone.motioncapture.NodeType;

public class HumanFrameCamera {
	public ArrayList<Cluster> listOfClusters;
	public double offSetX = 0;
	public double offSetY = 0;
	int humanHeightInPixel = 0;
	Node[] listOfNode = new Node[10];
	int nodeCounter = 0;
	public HumanFrameCamera() {
		listOfClusters = new ArrayList<Cluster>();
		nodeCounter = 0;
	}
	
/*	public int addHumanFrame(HumanFrame inputFrame, int currentCounter) {
		if(svmHumanTrajectory.getTotalFrames() >= 10){
			svmHumanTrajectory.removeFirst();
		}
		svmHumanTrajectory.addLast(inputFrame);
		currentCounter--;
		return currentCounter;
	}*/
	
	public void setHead(Cluster inputCluster){
		inputCluster.setType(ClusterType.HEAD);
		offSetX = inputCluster.centreOfClusterSupport.x;
		offSetY = inputCluster.centreOfClusterSupport.y;
		Node newNode = new Node(NodeType.HEAD, 0, 0);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 0){
			listOfClusters.add(0, inputCluster);
		}else{
			listOfClusters.set(0, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[0] = newNode;
		}
	}
	
	public void setLeftArm(Cluster inputCluster){
		inputCluster.setType(ClusterType.LEFT_ARM);
		Node newNode = new Node(NodeType.LEFT_ELBOW, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 1){
			listOfClusters.add(1, inputCluster);
		}else{
			listOfClusters.set(1, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[1] = newNode;
		}
	}
	
	private Node normalizeNode(Node newNode) {
		double normalizedX = 0, normalizedY = 0;
		if(humanHeightInPixel > 0){
			normalizedX = 1.0*(newNode.getX() - offSetX)/humanHeightInPixel;
			//normalizedY = 1.0*(newNode.getY() - offSetY)/humanHeightInPixel;
			normalizedY = 1.0*(offSetY - newNode.getY())/humanHeightInPixel;
		}
		return new Node(newNode.getNodeType(), normalizedX, normalizedY);
	}

	public void setLeftHand(Cluster inputCluster){
		inputCluster.setType(ClusterType.LEFT_HAND);
		Node newNode = new Node(NodeType.LEFT_HAND, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 2){
			listOfClusters.add(2, inputCluster);
		}else{
			listOfClusters.set(2, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[2] = newNode;
		}
	}

	public void setRightArm(Cluster inputCluster){
		inputCluster.setType(ClusterType.RIGHT_ARM);
		Node newNode = new Node(NodeType.RIGHT_ELBOW, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 3){
			listOfClusters.add(3, inputCluster);
		}else{
			listOfClusters.set(3, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[3] = newNode;
		}
	}
	
	public void setRightHand(Cluster inputCluster){
		inputCluster.setType(ClusterType.RIGHT_HAND);
		Node newNode = new Node(NodeType.RIGHT_HAND, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 4){
			listOfClusters.add(4, inputCluster);
		}else{
			listOfClusters.set(4, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[4] = newNode;
		}
	}
	
	public void setHip(Cluster inputCluster){
		inputCluster.setType(ClusterType.HIP);
		Node newNode = new Node(NodeType.HIP, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 5){
			listOfClusters.add(5, inputCluster);
		}else{
			listOfClusters.set(5, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[5] = newNode;
		}
	}
	
	public void setLeftKnee(Cluster inputCluster){
		inputCluster.setType(ClusterType.LEFT_KNEE);
		Node newNode = new Node(NodeType.LEFT_KNEE, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 6){
			listOfClusters.add(6, inputCluster);
		}else{
			listOfClusters.set(6, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[6] = newNode;
		}
	}
	
	public void setLeftFoot(Cluster inputCluster){
		inputCluster.setType(ClusterType.LEFT_FOOT);
		Node newNode = new Node(NodeType.LEFT_FOOT, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 7){
			listOfClusters.add(7, inputCluster);
		}else{
			listOfClusters.set(7, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[7] = newNode;
		}
	}
	
	public void setRightKnee(Cluster inputCluster){
		inputCluster.setType(ClusterType.RIGHT_KNEE);
		Node newNode = new Node(NodeType.RIGHT_KNEE, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 8){
			listOfClusters.add(8, inputCluster);
		}else{
			listOfClusters.set(8, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[8] = newNode;
		}
	}
	
	public void setRightFoot(Cluster inputCluster){
		inputCluster.setType(ClusterType.RIGHT_FOOT);
		Node newNode = new Node(NodeType.RIGHT_FOOT, inputCluster.centreOfClusterSupport.x, inputCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		inputCluster.updateAreaOfInterest(null, humanHeightInPixel);
		if(listOfClusters.size() == 9){
			listOfClusters.add(9, inputCluster);
		}else{
			listOfClusters.set(9, inputCluster);
		}
		if(nodeCounter < 10){
			listOfNode[9] = newNode;
		}
	}
	
	public Cluster getHead(){
		return listOfClusters.get(0);
	}
	
	public Cluster getLeftArm(){
		return listOfClusters.get(1);
	}
		
	public Cluster getLeftHand(){
		return listOfClusters.get(2);
	}
	
	public Cluster getRightArm(){
		return listOfClusters.get(3);
	}
	
	public Cluster getRightHand(){
		return listOfClusters.get(4);
	}
	
	public Cluster getHip(){
		return listOfClusters.get(5);
	}
	
	public Cluster getLeftKnee(){
		return listOfClusters.get(6);
	}
	
	public Cluster getLeftFoot(){
		return listOfClusters.get(7);
	}
	
	public Cluster getRightKnee(){
		return listOfClusters.get(8);
	}
	
	public Cluster getRightFoot(){
		return listOfClusters.get(9);
	}

	public void setHeightInPixel(int humanHeightInPixel) {
		this.humanHeightInPixel = humanHeightInPixel;		
	}

	public void resetCounter() {
		nodeCounter = 0;		
	}

	public void updateAllNodes() {
		//setLeftArm(getLeftArm());
		//setLeftHand(getLeftHand());
		//setRightArm(getRightArm());
		//setRightHand(getRightHand());
		Cluster elementCluster = getLeftArm();
		Node newNode = new Node(NodeType.LEFT_ELBOW, elementCluster.centreOfClusterSupport.x, elementCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		listOfNode[1] = newNode;
				
		elementCluster = getLeftHand();
		newNode = new Node(NodeType.LEFT_HAND, elementCluster.centreOfClusterSupport.x, elementCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		listOfNode[2] = newNode;
		
		elementCluster = getRightArm();
		newNode = new Node(NodeType.RIGHT_ELBOW, elementCluster.centreOfClusterSupport.x, elementCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		listOfNode[3] = newNode;
		
		elementCluster = getRightHand();
		newNode = new Node(NodeType.RIGHT_HAND, elementCluster.centreOfClusterSupport.x, elementCluster.centreOfClusterSupport.y);
		newNode = normalizeNode(newNode);
		listOfNode[4] = newNode;
		
		setHip(getHip());
		setLeftKnee(getLeftKnee());
		setLeftFoot(getLeftFoot());
		setRightKnee(getRightKnee());
		setRightFoot(getRightFoot());		
	}
}

