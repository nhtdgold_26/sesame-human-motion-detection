package sesame.actiondetection;

import sesame.HumanAction;

public interface SinglePhoneHumanMotionListener {
	public void onActionDetected(HumanAction detectedAction);
}
