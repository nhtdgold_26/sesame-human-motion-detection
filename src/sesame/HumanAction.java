package sesame;

public enum HumanAction {
	RaiseLeftHand, RaiseRightHand, Kick, None
}
