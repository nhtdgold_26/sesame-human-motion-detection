package Controller;

import android.app.Activity;

import com.example.sesame_social_gaming_project.PauseResumeListener;

public class Controller implements PauseResumeListener {
	private Activity act;
	public boolean performConnectionDiscovery = true;
	
	private CompabilityChecker compabilityChecker;
	
	
	public Controller(Activity act){
		this(act,new CompabilityChecker());
	}
	
	public Controller(Activity act, CompabilityChecker cc) {
		this.compabilityChecker = cc;
		this.act = act;
	}
	
	public boolean isCompatible(){
		return compabilityChecker.check();
	}
	
	public void onResume(){
	}
	
	public void onPause(){
	}
}